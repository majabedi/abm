# Created by Majid Abedi(majabedi@gmail.com) on 14/03/2020
# This file is intended to create the object to store the statistics of a common place, called node.
# The agents are assigned to a node at all the rimes and all the agents on one node share the same statistics.

import random
import math
import sys
import numpy as np

from person import Status
from read_outs import get_all_status_to_report
from read_outs import get_unique_name


class node:
    """
        A class used to store all properties for one node and the possible markov transitions.
    """

    def __init__(self, name, capacity, duration, max_allowed_duration, markov_parameters, node_effective_contacts, initial_populations, death_rate_reduction_factor = 1 , age_dependent_rates = None, is_parent = False, individual_interaction_considered = False):
        """
        initializes the object
        :param name: name of the node as string
        :param capacity: capacity of the node
        :param duration: minimum duration required for agents to stay on this node in days
        :param max_allowed_duration: maximum time that agents can spend on this node in days
        :param markov_parameters: markov transition rates
        :param node_effective_contacts: number of effective contacts in the node
        :param initial_populations: initial number of each population
        :param death_rate_reduction_factor: how much the death rate should reduce in this node
        """
        self.name = name
        sub_populations = {}
        total = 0
        for s in get_all_status_to_report():
            sub_populations[s] = 0
        self.size = total
        self.sub_populations = sub_populations
        self.capacity = capacity
        self.w_sc_i = markov_parameters['w_sc_i']
        self.w_sc_c = markov_parameters['w_sc_c']
        self.age_dependent_rates = age_dependent_rates
        self.time_duration = duration
        self.node_effective_contacts = node_effective_contacts
        self.death_rate_reduction_factor = death_rate_reduction_factor
        self.should_calculte_rates = True
        self.reset_statistics()
        self.max_allowed_duration = max_allowed_duration
        self.ID = random.randint(0, sys.maxsize)
        self.is_parent=is_parent
        self.individual_interaction_considered = individual_interaction_considered
        if (individual_interaction_considered):
            self.people=[]
        self.transitions = markov_parameters['transitions']
        self.counted_contacts = 0
        return

    def get_name(self):
        """

        :return: the name of node
        """
        return self.name

    def get_size(self):
        """

        :return: the whole population size
        """
        return self.size

    def get_subpopulation(self, status):
        """

        :param status: status
        :return: the number of agents
        """
        return self.sub_populations[status]

    def get_statistics(self, status):
        """
        returns the statistics of new cases of one status
        :param status: status
        :return: the statistics of new cases
        """
        return self.statistics[status]

    def get_time_duration(self):
        """
        returns the minimum time to stay in the node
        :return: minimum time
        """
        return self.time_duration

    def get_max_allowed_duration(self):
        """
        returns the maximum time to allow an agent stay in the node
        :return: maximum allowed time
        """
        return self.max_allowed_duration

    def one_changes_status(self, old_status, new_status):
        """
        inform the node that one agent has changed the infection status
        :param old_status: old status
        :param new_status: new status
        :return:
        """
        if (not self.individual_interaction_considered):
            self.should_calculte_rates = True
            # To be able for the next agents in the infection row to face the true rates and be able to pick up the correct transmitter we need to recalculate_the_rates
            if ( old_status == Status.Carrier or old_status == Status.Infected_Detected or old_status == Status.Infected_Undetected
                    or new_status == Status.Carrier or new_status == Status.Infected_Undetected or new_status == Status.Infected_Detected):
                self.calculate_transition_rate_matrix()

        self.sub_populations[old_status] -= 1
        self.sub_populations[new_status] += 1
        if(old_status!=new_status):
            self.statistics[new_status] += 1
        if (self.sub_populations[old_status] < 0):
            raise RuntimeError('population of ' + old_status + ' got negative in node ' + self.get_name())
        if (self.sub_populations[new_status] > self.capacity):
            raise RuntimeError('population of ' + new_status + ' got more than capacity in node ' + self.get_name())
        # TODO. Consider it once we have different time scales.
        return

    def change_one_subpopulation_and_total(self, status, delta):
        """
        if one agent has migrated from/to the node the statustics should change.
        :param status: the status
        :param delta: change in the corresponding population
        :return:
        """
        self.sub_populations[status] += delta
        self.size += delta
        if (not self.individual_interaction_considered):
            self.should_calculte_rates = True
        #This step might be possible to do once after all migrations
        # TODO. Consider it once we have different time scales.
        # self.calculate_transition_rate_matrix()
        return

    def get_node_effective_contacts(self):
        return min(self.node_effective_contacts,self.get_size()-1)

    def next_markov_state(self, from_status, delta_t):
        """
        produces a random variable and gives the next markov state according to the time step and the markov rates
        :param from_status: the current infection status
        :param delta_t: the time step in days
        :return: the next infection status
        """
        #Transition Rates are independent of delta_t
        transition_rates = self.transition_matrix[from_status].copy()

        for to_status in sorted(list(transition_rates.keys()), key=lambda x:x.value):
            transition_rates[to_status] *= delta_t

        transition_rates[from_status]+=1.

        w = random.uniform(0.0, 1.0)
        cumsum = 0



        for to_status in sorted(list(transition_rates.keys()), key=lambda x:x.value):
            cumsum += transition_rates[to_status]
            if (w < cumsum):
                return to_status
        if (cumsum > 1):
            raise RuntimeError("Cumulative sum of transition probabilities for " + from_status.name + " bigger than 1.")

        return

    def reset_statistics(self):
        """
        resets the statistics
        :return:
        """
        statistics = {}
        for s in get_all_status_to_report():
            statistics[s] = 0
        self.statistics = statistics
        return

    def interact_with_infecteds(self, person):
        if (not self.individual_interaction_considered):
            raise RuntimeWarning('This code is optimized for individual interactions')
            return self.next_markov_state_and_time(Status.Susceptible ,person.get_age())
        agents_excluding_person = [x for x in self.people if x != person]
        if (int(self.node_effective_contacts)==self.node_effective_contacts):
            contacts_to_make = int(self.node_effective_contacts)
        else:
            contacts_to_make = int(self.node_effective_contacts) + np.random.binomial(1, self.node_effective_contacts - int(self.node_effective_contacts))

        intercting_people = random.sample(agents_excluding_person ,min(contacts_to_make,self.get_size()-1))

        infected_interacting_v1 = [p for p in intercting_people if (p.get_status() == Status.Infected_Detected
                                                                      or p.get_status() == Status.Infected_Undetected)  and p.get_virus_variant()==1 ]
        carrier_interacting_v1 = [p for p in intercting_people if (p.get_status() == Status.Carrier)  and p.get_virus_variant()==1 ]

        infected_interacting_v2 = [p for p in intercting_people if ((p.get_status() == Status.Infected_Detected
                                                                     or p.get_status() == Status.Infected_Undetected ) and p.get_virus_variant() == 2)]
        carrier_interacting_v2 = [p for p in intercting_people if (p.get_status() == Status.Carrier) and p.get_virus_variant()==2 ]
        
        infected_interacting_v3 = [p for p in intercting_people if ((p.get_status() == Status.Infected_Detected
                                                                     or p.get_status() == Status.Infected_Undetected ) and p.get_virus_variant() == 3)]
        carrier_interacting_v3 = [p for p in intercting_people if (p.get_status() == Status.Carrier) and p.get_virus_variant()==3 ]


        person.get_node().increase_counted_contacts(len(intercting_people))
        wi1=self.w_sc_i * len(infected_interacting_v1)
        wc1=self.w_sc_c * len(carrier_interacting_v1)
        wi2=self.w_sc_i * len(infected_interacting_v2)*1.7
        wc2=self.w_sc_c * len(carrier_interacting_v2)*1.7
        
        wi3=self.w_sc_i * len(infected_interacting_v3)*1.7*1.5
        wc3=self.w_sc_c * len(carrier_interacting_v3)*1.7*1.5
        
        rate_sc = wi1 + wc1 + wi2 + wc2 + wi3 + wc3
        if (rate_sc==0):
            #basically means it will never happen
            dt=sys.maxsize
            _transmitter=None
        else:
            r=random.uniform(0.0, 1.0)
            if(rate_sc>r):
                #For simplicity we keep it as it was, before there was an exponential random number generated
                #if the random number was very small, this was happening. This is basically the same thing
                #meaning that dt=0 is the same as the event happning
                dt = 0
            else:
                dt=1000000
            w=random.uniform(0.0, 1.0)
            if (w<wi1/rate_sc):
                _transmitter = random.sample(infected_interacting_v1,1)[0]
            else:
                if(w<(wi1+wc1)/rate_sc):
                    _transmitter = random.sample(carrier_interacting_v1,1)[0]
                else:
                    if(w<(wi1+wc1+wi2)/rate_sc):
                        _transmitter = random.sample(infected_interacting_v2, 1)[0]
                    else:
                        if(w<(wi1+wc1+wi2+wc2)/rate_sc):
                            _transmitter = random.sample(carrier_interacting_v2, 1)[0]
                        else:
                            if(w<(wi1+wc1+wi2+wc2+wi3)/rate_sc):
                                _transmitter = random.sample(infected_interacting_v3, 1)[0]
                            else:
                                _transmitter = random.sample(carrier_interacting_v3, 1)[0]
  

        return Status.Exposed, dt, _transmitter

    def interact_infect_others(self, person , factor=1.0):
        agents_excluding_person = [x for x in self.people if x != person]
        if (int(self.node_effective_contacts*factor)==self.node_effective_contacts*factor):
            contacts_to_make = int(self.node_effective_contacts*factor)
        else:
            contacts_to_make = int(self.node_effective_contacts*factor) + np.random.binomial(1, self.node_effective_contacts*factor - int(self.node_effective_contacts*factor))

        if (contacts_to_make<=self.get_size() - 1):
            interacting_people_number = contacts_to_make
        else:
            interacting_people_number = self.get_size() - 1
            #print('Number of contacts that the infected agent %s wants to make in node %s is(%d) and is more than the '
            #      'people(%d) in the node'%(person.get_ID(),self.name,contacts_to_make,self.get_size() - 1))
        intercting_people = random.sample(agents_excluding_person ,interacting_people_number)

        person.get_node().increase_counted_contacts(interacting_people_number)

        susceptible_interacting = [p for p in intercting_people if p.get_status() == Status.Susceptible ]

        if (len(susceptible_interacting)==0):
            return [], intercting_people
        else:
            individual_contact_transmission_probablity = self.w_sc_c
            if (person.get_virus_variant()==2):
                individual_contact_transmission_probablity *= 1.5 #For the VOC the transmission probablity is 1.5 times the ordinary variant
            infectees = [s for s in susceptible_interacting if random.uniform(0.0, 1.0)< individual_contact_transmission_probablity]
            if(self.w_sc_c!=self.w_sc_i):
                raise RuntimeError('w_sc_c must be equal to w_sc_i otherwise you need t change the code')
            return infectees, intercting_people





############################################################
    def calculate_transition_rate_matrix(self):
        if not self.should_calculte_rates:
            return
        """
        In this function we define the markov transitions for the node.
        This function is called from person.py
        Note that here only the transition to other states are considered and not to self,
        and it is not dependent of time step.

        :return:
        """
        self.transition_matrix={}
        if (self.age_dependent_rates is not None):
            for age in self.age_dependent_rates.index.values:
                w_sc_i = self.w_sc_i
                w_sc_c = self.w_sc_c
                w_ir = self.w_ir
                w_id = self.w_id
                w_rs = self.w_rs
                w_ci = self.w_ci
                w_cr = self.w_cr
                mu_detected_infected = self.mu_detected_infected
                matrix = {}
                for col in self.age_dependent_rates.columns:
                    if (col == 'w_sc_i'):
                        w_sc_i *= self.age_dependent_rates.loc[age][col]
                    elif (col == 'w_sc_c'):
                        w_sc_c *= self.age_dependent_rates.loc[age][col]
                    elif (col == 'w_ir'):
                        w_ir *= self.age_dependent_rates.loc[age][col]
                    elif (col == 'w_id'):
                        w_id *= self.age_dependent_rates.loc[age][col]
                    elif (col == 'w_rs'):
                        w_rs *= self.age_dependent_rates.loc[age][col]
                    elif (col == 'w_ci'):
                        w_ci *= self.age_dependent_rates.loc[age][col]
                    elif (col == 'w_cr'):
                        w_cr *= self.age_dependent_rates.loc[age][col]
                    elif (col == 'mu_detected_infected'):
                        mu_detected_infected *= self.age_dependent_rates.loc[age][col]


                # rate of Susceptible to Carrier
                total = self.get_size()
                #To avoid division by zero
                if(total==0):
                    total=1
                rate_sc = w_sc_i * float( self.get_subpopulation(Status.Infected_Undetected)
                                     + self.get_subpopulation(Status.Infected_Detected)
                                     ) + \
                      w_sc_c * float(self.get_subpopulation(Status.Carrier))

                rate_sc *= min(float(self.node_effective_contacts)/total,1.)

                # rate of Infected to Recovered
                rate_ir = w_ir

                # rate of Infected to Dead
                rate_id = w_id

                rate_id*=self.death_rate_reduction_factor

                # rate of Recovered to Susceptible
                rate_rs = w_rs

                # rate of Carrier to Recovered
                rate_cr = w_cr

                # rate of Carrier to Detected Infected
                rate_ci_d = w_ci * mu_detected_infected

                # rate of Carrier to UnDetected Infected
                rate_ci_u = w_ci * (1 - mu_detected_infected)

                matrix[Status.Susceptible] = {
                    Status.Exposed: rate_sc,
                }

                matrix[Status.Carrier] = {
                    Status.Infected_Detected: rate_ci_d,
                    Status.Infected_Undetected: rate_ci_u,
                    Status.Recovered_C: rate_cr,
                }

                matrix[Status.Infected_Undetected] = {
                    Status.Recovered_I: rate_ir,
                    Status.Dead: rate_id
                }

                matrix[Status.Infected_Detected] = {
                    Status.Recovered_I: rate_ir,
                    Status.Dead: rate_id
                }

                matrix[Status.Recovered_C] = {
                    Status.Susceptible: rate_rs,
                }

                matrix[Status.Recovered_I] = {
                    Status.Susceptible: rate_rs,
                }

                self.transition_matrix[age] = matrix
        else:
            w_sc_i = self.w_sc_i
            w_sc_c = self.w_sc_c
            w_ir = self.w_ir
            w_id = self.w_id
            w_rs = self.w_rs
            w_ci = self.w_ci
            w_cr = self.w_cr
            mu_d = self.mu_detected_infected
            matrix = {}
            # rate of Susceptible to Carrier
            total = self.get_size()
            # To avoid division by zero
            if (total == 0):
                total = 1
            rate_sc = w_sc_i * float( self.get_subpopulation(Status.Infected_Undetected)
                                     + self.get_subpopulation(Status.Infected_Detected)
                                     ) + \
                      w_sc_c * float(self.get_subpopulation(Status.Carrier))

            rate_sc *= min(float(self.node_effective_contacts)/total,1.)

            # rate of Infected to Recovered
            rate_ir = w_ir

            # rate of Infected to Dead
            rate_id = w_id

            rate_id *= self.death_rate_reduction_factor

            # rate of Recovered to Susceptible
            rate_rs = w_rs

            # rate of Carrier to Recovered
            rate_cr = w_cr

            # rate of Carrier to Detected Infected
            rate_ci_d = w_ci * mu_d

            # rate of Carrier to UnDetected Infected
            rate_ci_u = w_ci * ( 1-mu_d )

            matrix[Status.Susceptible] = {
                Status.Exposed: rate_sc,
            }

            matrix[Status.Carrier] = {
                Status.Infected_Detected: rate_ci_d,
                Status.Infected_Undetected: rate_ci_u,
                Status.Recovered_C: rate_cr,
            }

            matrix[Status.Infected_Undetected] = {
                Status.Recovered_IU: rate_ir,
                Status.Dead: rate_id
            }

            matrix[Status.Infected_Detected] = {
                Status.Recovered_ID: rate_ir,
                Status.Dead: rate_id
            }

            

            
            self.transition_matrix = matrix
        self.should_calculte_rates = False
        return

    def next_markov_state_and_time(self, from_status, age):
        if (self.age_dependent_rates is None):
            return self.next_markov_state_and_time_without_age(from_status)
        age=int(age)
        
        if(from_status not in self.transition_matrix[age].keys()):
            return from_status,sys.maxsize

        transition_rates = self.transition_matrix[age][from_status]

        W=0
        for to_status in sorted(list(transition_rates.keys()), key=lambda x:x.value):
            W += transition_rates[to_status]
        if (W==0):
            return from_status,sys.maxsize

        dt = -math.log(random.uniform(0.0, 1.0)) / W

        w = random.uniform(0.0, 1.0)
        cumsum = 0
        for to_status in sorted(list(transition_rates.keys()), key=lambda x:x.value):
            cumsum += transition_rates[to_status]/W
            if (w < cumsum):
                return to_status, dt
        if (cumsum > 1):
            raise RuntimeError("Cumulative sum of transition probabilities for " + from_status.name + " bigger than 1.")

    def update_next_state(self, person):
        w = random.uniform(0.0, 1.0)
        cumsum = 0

        #to which stated it can go
        if(person.get_status() not in self.transitions.keys()):
            return person.get_status(),sys.maxsize

        possible_destinations=self.transitions[person.get_status()]

        for to_status in sorted(list(possible_destinations.keys()), key=lambda x:x.value):
            cumsum += possible_destinations[to_status]["fraction"]
            if (w < cumsum):
                #EXCEPTION
                if(person.get_status()==Status.Carrier):
                    dt = 2.0/3.2*person.get_time_supposed_to_stay_in_infection_state()
                    #Exception new addition to Carrier state time on 7.11.2020
                    #To consider a delay of knowing the test results can be given here
                    infected_protected_period=0.0
                    if (to_status == Status.Infected_Detected):
                        dt = dt + infected_protected_period
                    return to_status, dt

                if (possible_destinations[to_status]["distribution"]=='gaussian'):
                    mu=possible_destinations[to_status]["mu"]
                    sigma=possible_destinations[to_status]["sigma"]
                    dt=np.random.normal(mu, sigma)
                    return to_status, dt

                if (possible_destinations[to_status]["distribution"]=='lognormal'):
                    mu=possible_destinations[to_status]["mu"]
                    sigma=possible_destinations[to_status]["sigma"]
                    dt=np.random.lognormal(mu, sigma)
                    #Exception for Exposed State added on 7.11.2020
                    #Since the distribution for the whole incubation period and it is then divided to Exposed and Carrier states we should choose a random number and then divide it to two numbers with specific(3.2:2.0) proportions
                    if (person.get_status() == Status.Exposed):
                        dt = 3.2 / 5.2 * dt
                    return to_status, dt

                if (possible_destinations[to_status]["distribution"]=='weibull'):
                    k=possible_destinations[to_status]["k"]
                    lam=possible_destinations[to_status]["lambda"]
                    dt=np.random.weibull(k)*lam
                    return to_status, dt

                if (possible_destinations[to_status]["distribution"]=='exponential'):
                    beta=possible_destinations[to_status]["lambda"]
                    dt=np.random.exponential(beta)

                return to_status, dt

    def next_markov_state_and_time_without_age(self, from_status):

        #Transition Rates are independent of delta_t
        if(from_status not in self.transition_matrix.keys()):
            return from_status,sys.maxsize

        transition_rates = self.transition_matrix[from_status]

        W=0
        for to_status in sorted(list(transition_rates.keys()), key=lambda x:x.value):
            W += transition_rates[to_status]
        if (W==0):
            return from_status,sys.maxsize

        dt = -math.log(random.uniform(0.0, 1.0)) / W

        w = random.uniform(0.0, 1.0)
        cumsum = 0
        for to_status in sorted(list(transition_rates.keys()), key=lambda x:x.value):
            cumsum += transition_rates[to_status]/W
            if (w < cumsum):
                return to_status, dt
        if (cumsum > 1):
            raise RuntimeError("Cumulative sum of transition probabilities for " + from_status.name + " bigger than 1.")


    def get_ID(self):
        """

        :return: ID of the node
        """
        return self.ID
    def is_parent_node(self):
        return self.is_parent

    def get_transmittion_from_infected_probablity(self):
        return self.w_sc_i * float(self.get_subpopulation(Status.Infected_Undetected)
                       + self.get_subpopulation(Status.Infected_Detected) )

    def get_transmittion_from_carrier_probablity(self):
        return self.w_sc_c * float(self.get_subpopulation(Status.Carrier))

    def update_lists_agent_arrived(self, person):
        if (self.individual_interaction_considered):
            self.people.append(person)
        self.change_one_subpopulation_and_total(get_unique_name(person.status, person.get_virus_variant()), +1)

    def update_lists_agent_left(self, person):
        if (self.individual_interaction_considered):
            self.people.remove(person)
        self.change_one_subpopulation_and_total(get_unique_name(person.status, person.get_virus_variant()), -1)


    def scale_all_contacts_frequency(self,a):
        self.node_effective_contacts*=a
        return

    def reset_counted_contacts(self):
        self.counted_contacts=0
        return

    def increase_counted_contacts(self, n):
        self.counted_contacts += n
        return

    def get_counted_contacts(self):
        return self.counted_contacts


