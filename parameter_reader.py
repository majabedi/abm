# Created by Majid Abedi(majabedi@gmail.com) on 12/03/2020
# The parameter reader reads the simulation parameters and passes them as a dictionary.

def read_parameters(parameter_file):
    """
    reads the parameters in the provided file
    it ignores spaces, tab characters and everything after #
    :param parameter_file:
    :return: a dictionary of parameter names and their string values
    """
    parameters = {}
    with open(parameter_file) as f:
        for line in f:
            line = line.replace(' ', '')
            line = line.replace('\t', '')
            line = line.replace('\n', '')
            if ((not line.startswith('#') and line)):
                line_without_comment = line.split("#")[0]
                [parameter, value] = line_without_comment.split("=")
                parameters[parameter] = value
                print('Set %s = %s' % (parameter, value))
    return parameters
