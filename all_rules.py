# Created by Majid Abedi(majabedi@gmail.com) on 23/03/2020
# This file includes the rules to be imported and used for agents migration.
# The order of rules is important. The rules are applied in the provided order.

from rulesgathered import rule1,rule2,rule4,rule7,rule9,rule6,self_isolate,travel,quarantine_rule_first_degree_contacts

rules=[
    quarantine_rule_first_degree_contacts,
    rule6,
    rule1,
    rule2,
    rule4,
    rule7,
    rule9
]

node_independent_rules=[
    quarantine_rule_first_degree_contacts,
]
