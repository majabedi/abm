# Created by Anastasios Siokis(anastasiossio@gmail.com) on 24/03/2020
# This file is intended to include all the rules which are applied to the agents to migrate.

import random
import sys
from datetime import datetime
import numpy as np

from person import Status

# Dates of possible lockdowns
lockdown_school_date      =datetime(2020, 3, 15)
lockdown_se_date          =datetime(2020, 3, 20) # social event
lockdown_be_date          =datetime(2020, 3, 1)  # big event
lockdown_shopping_date    =datetime(2020, 3, 20)
lockdown_rcb_date         =datetime(2020, 3, 20) # RestaurantCafeBar
lockdown_church_date      =datetime(2020, 3, 20)
lockdown_wp_date          =datetime(2020, 3, 1)
lockdown_sm_date          =datetime(2020, 2, 10)
elderly_stay_home_date    =datetime(2020, 3, 15)

reopening_school_date     =datetime(2020, 5, 4)
reopening_se_date         =datetime(2020, 4, 20)
reopening_be_date         =datetime(2020, 3, 13)
reopening_shopping_date   =datetime(2020, 5, 4)
reopening_rcb_date        =datetime(2020, 5, 11) # RestaurantCafeBar
reopening_church_date     =datetime(2020, 5, 6)
reopening_wp_date         =datetime(2020, 3, 5)
reopening_sm_date         =datetime(2020, 12, 20)
elderly_go_out_again_date =datetime(2020, 12, 20)

school_summer_vacation_start    =datetime(2020, 8, 12) # Niedersachsen
school_summer_vacation_end      =datetime(2020, 8, 23)

test_result_period = 2

def give_me_random_hour(person, min_hour, max_hour):
    #divide by 1000000 to make it independent of other random numbers generated based on the ID
    return int(person.get_ID()/1000000) % (max_hour-min_hour+1) + min_hour

# Application of lockdowns

#########################################
# Rule 1.
# people <=20 or >=65 do not go the Workplace and people >=65 do not go to School node
# The people between 20 and 65 can go everywhere exept for School
# t.hour gives the hour of the day
# t.weekday() gives the week day which ranges from 0(Monday) to 6 (Sunday)
def rule1(person,t,delta_t):

    rule_applied = False
    destination = person.get_node()
    #TODO: since the rules are designed only for one hour time steps they do not work with lower time steps.
    if (t.minute != 0):
        return rule_applied, destination

    # Destinations that everyone can take, School and Work Place happen independently
    # make social event independent like school and workplace
    possible_destination = ["Home", "Home", "Home", "Home", "Super Market", "Social Event", "RestaurantCafeBar", "Shopping"]
    possible_destination_sunday = ["Home", "Home", "Home", "Social Event", "RestaurantCafeBar"]
    possible_destination_sunday_morning = ["Home","Home","Home","Home"]
    evening_out_destination = ["Home", "Home", "Home", "Social Event", "RestaurantCafeBar"]
    big_event_destination = ["Home", "Home", "Home", "Home", "Big Event"]

###### Lockdowns ###########################################
    lockdown_school   = True
    lockdown_se       = True
    lockdown_be       = True
    lockdown_shopping = True
    lockdown_rcb      = True
    lockdown_church   = True
    #partial lockdown
    #p_wp_ld is the probablity of having lockdown
    #1 is full lock down
    #0 is full open
    p_wp_ld=0.25
    person_ID_rnd = person.get_ID()/sys.maxsize
    lockdown_wp     = (p_wp_ld>person_ID_rnd)
    lockdown_sm     = False

    ###### Reopening  ##########################################
    reopening_school   = True
    reopening_se       = True
    reopening_be       = False
    reopening_shopping = True
    reopening_rcb      = True
    reopening_church   = True
    reopening_wp       = True
    reopening_sm       = False

    if (t>reopening_school_date and reopening_school):
        lockdown_school = False
    if (t>reopening_se_date and reopening_se):
        lockdown_se = False 
    if (t>reopening_be_date and reopening_be):
        lockdown_be = False 
    if (t>reopening_shopping_date and reopening_shopping):
        lockdown_shopping = False
    if (t>reopening_rcb_date and reopening_rcb):
        lockdown_rcb = False
    if (t>reopening_church_date and reopening_church):
        lockdown_church = False 
    if (t>reopening_wp_date and reopening_wp):
        lockdown_wp= False  
    if (t>reopening_sm_date and reopening_sm):
        lockdown_sm = False
############################################################

    if (t>lockdown_rcb_date and lockdown_rcb):
        possible_destination.remove("RestaurantCafeBar")
        possible_destination.append("Home")
        possible_destination_sunday.remove("RestaurantCafeBar")
        possible_destination_sunday.append("Home")
        evening_out_destination.remove("RestaurantCafeBar")
        evening_out_destination.append("Home")

    if (t>lockdown_shopping_date and lockdown_shopping):
        possible_destination.remove("Shopping")
        possible_destination.append("Home")

    if (t>lockdown_church_date and lockdown_church):
        possible_destination_sunday_morning.remove("Church")
        possible_destination_sunday_morning.append("Home")
        
    if (t>lockdown_se_date and lockdown_se):
        possible_destination.remove("Social Event")
        possible_destination.append("Home")
        possible_destination_sunday.remove("Social Event")
        possible_destination_sunday.append("Home")
        evening_out_destination.remove("Social Event")
        evening_out_destination.append("Home")

    if (t>lockdown_be_date and lockdown_be):
        big_event_destination.remove("Big Event")
        big_event_destination.append("Home")

    if (t>lockdown_sm_date and lockdown_sm):
        possible_destination.remove("Super Market")
        possible_destination.append("Home")     
############################################################

    # According to the meeting on 21.09 we decided that the people would stay at home for quarantine period
    # but only one person and not the whole household. So there are 3 flags for quarantine:
    # 1. quarantine
    # 2. household_quarantine
    # 3. travel_quarantine
    if (person.is_self_isolated() and person.get_node().get_name() == "Home"):
        return rule_applied, destination

    if (person.is_travel_quarantined() and person.get_node().get_name() == "Home"):
        return rule_applied, destination

    if (person.is_first_degree_contact_quarantined() and person.get_node().get_name() == "Home"):
        return rule_applied, destination

    if (person.get_status()==Status.Dead):
        return rule_applied, destination

    if (person.get_node().get_name() == "Hospital" or person.get_node().get_name() == "ICU"):
        return rule_applied, destination 

    #if the person is Detected Infected, this rule will not apply
    #According to the meeting on 21.09 we decided that staying at home applies only if there is quarantine which is now
    #applied in rule stay_home_quarantine_rule
    #if (person.get_status()==Status.Infected_Detected):
    #    return rule_applied, destination

    #if the person is in a quarantined household, and is at home, it stays there
    if (person.is_household_quarantined('Home') and person.get_node().get_name()== "Home"):
        return rule_applied, destination
    min_age = 0
    max_age = 20
    if(person.get_age()>=min_age and person.get_age()<=max_age):
        # make a random time to go to school based on the ID
        gotoSchool    = give_me_random_hour(person,7,9)
        # and go home 6 hours later
        leaveSchool   = gotoSchool + 6
        saturdayOut   = give_me_random_hour(person,9,19)
        sundayMorning = give_me_random_hour(person,9,11)
        sundayOut     = give_me_random_hour(person,11,19)
        bigEvent      = give_me_random_hour(person,14,19)
        if (t.hour<7):
            destination = 'Home'
            rule_applied = True
            return rule_applied, destination
#================= MONDAY - FRIDAY ======================================================================================================================#
        if (t.weekday()<5):
            if (lockdown_school==False and not(t>=school_summer_vacation_start and t<=school_summer_vacation_end)):     
                if (t.hour==gotoSchool): # For lockdown -> if (t.hour==gotoSchool and t.weekday()<5 and lockdownschool=False):
                    destination = 'School'
                    rule_applied = True
                    return rule_applied, destination 
                if (t.hour==leaveSchool):
                    destination = random.choice(possible_destination)
                    rule_applied = True
                    return rule_applied, destination
            if (t<lockdown_school_date and lockdown_school) and (not (t>school_summer_vacation_start and t<school_summer_vacation_end)): # exactly the same with above...
                if (t.hour==gotoSchool):
                    destination = 'School'
                    rule_applied = True 
                    return rule_applied, destination    
                if (t.hour==leaveSchool):
                    destination = random.choice(possible_destination)
                    rule_applied = True
                    return rule_applied, destination    
            if (t>lockdown_school_date and lockdown_school):
                if ((t.hour==saturdayOut) and  t.weekday()<5):
                    destination = random.choice(possible_destination)
                    rule_applied = True
                    return rule_applied, destination    
#================= SATURDAY =============================================================================================================================#
        if (t.weekday()==5):
            if ((t.hour==saturdayOut)):        
                destination = random.choice(possible_destination)
                rule_applied = True
                return rule_applied, destination               
#================= SUNDAY ===============================================================================================================================#
        if (t.weekday()==6):
            if ((t.hour==sundayMorning)):        
                destination = random.choice(possible_destination_sunday_morning)# Home, SE
                rule_applied = True 
                return rule_applied, destination                                      
            if ((t.hour==sundayOut)):        
                destination = random.choice(possible_destination_sunday)# Home, SE
                rule_applied = True 
                return rule_applied, destination   
        if (t.weekday()>3):
            if ((t.hour==bigEvent)):
                destination = random.choice(big_event_destination)# Home, SE
                rule_applied = True 
                return rule_applied, destination 
    else:
        if (person.get_age()>=65.0):
            goOut = give_me_random_hour(person,11,19)
            sundayMorning = give_me_random_hour(person,9,11)
            bigEvent      = give_me_random_hour(person,14,19)
            if (t.hour<7):
                destination = 'Home'
                rule_applied = True
                return rule_applied, destination     
#================= MONDAY - SATURDAY ====================================================================================================================#
            if (t.hour==goOut and t.weekday()<6):
                destination = random.choice(possible_destination)
                rule_applied = True
                return rule_applied, destination    
#================= SUNDAY ===============================================================================================================================#
            if (t.hour==sundayMorning and t.weekday()==6):
                destination = random.choice(possible_destination_sunday_morning)
                rule_applied = True
                return rule_applied, destination 
            if (t.hour==goOut and t.weekday()==6):
                destination = random.choice(possible_destination_sunday)
                rule_applied = True
                return rule_applied, destination    
            if (t.hour>=20):
                destination = 'Home'
                rule_applied = True
                return rule_applied, destination  
            if (t.weekday()>3):
                if (t.hour==bigEvent):
                    destination = random.choice(big_event_destination)# Home, SE
                    rule_applied = True 
                    return rule_applied, destination                   
        else:
            if (person.get_age()>20.0 and person.get_age()<65.0): 
                gotoWork = give_me_random_hour(person,6, 9)
                leaveWork = gotoWork + 9
                eveningOut = give_me_random_hour(person,20, 23)
                dayOut = give_me_random_hour(person,9,20)
                sundayMorning = give_me_random_hour(person,9,11)
                sundayOut =  give_me_random_hour(person,11,20)
                bigEvent      = give_me_random_hour(person,14,9)
                if (t.hour<6):
                    destination = 'Home'
                    rule_applied = True
                    return rule_applied, destination    
#================= MONDAY - SATURDAY ====================================================================================================================#
                if (t.weekday()<6):
                    if (lockdown_wp==False):    
                        if (t.hour==gotoWork):
                            destination = 'Work Place'
                            rule_applied = True
                            return rule_applied, destination     
                        if (t.hour==leaveWork):
                            destination = random.choice(possible_destination)
                            rule_applied = True
                            return rule_applied, destination    
                    if (t<lockdown_wp_date and lockdown_wp):
                        if (t.hour==gotoWork):
                            destination = 'Work Place'
                            rule_applied = True 
                            return rule_applied, destination    
                        if (t.hour==leaveWork):
                            destination = random.choice(possible_destination)
                            rule_applied = True
                            return rule_applied, destination    
                    if (t>lockdown_wp_date and lockdown_wp):
                        if (t.hour==dayOut):
                            destination = random.choice(possible_destination)
                            rule_applied = True
                            return rule_applied, destination    
                    if (t.hour==eveningOut):
                        destination = random.choice(evening_out_destination) # Home, SE
                        rule_applied = True
                        return rule_applied, destination    
#================= SUNDAY ===============================================================================================================================#
                if (t.weekday()==6):
                    if (t.hour==sundayMorning):
                        destination = random.choice(possible_destination_sunday_morning)
                        rule_applied = True
                        return rule_applied, destination    
                    if (t.hour==sundayOut):
                        destination = random.choice(possible_destination_sunday)
                        rule_applied = True
                        return rule_applied, destination    
                    if (t.hour>sundayOut):
                        destination = 'Home'
                        rule_applied = True
                        return rule_applied, destination
                if (t.weekday()>3):
                    if ((t.hour==bigEvent)):
                        destination = random.choice(big_event_destination)# Home, SE
                        rule_applied = True 
                        return rule_applied, destination     
    return rule_applied, destination

#########################################

#########################################
# Rule 2.
# #The people who are Detected Infected go directly to hospital
def rule2(person,t,delta_t):
    # rates are from Ferguson, Neil, et al. "Report 9: Impact of non-pharmaceutical interventions (NPIs) to reduce COVID19 mortality and healthcare demand." (2020).
    # https://www.imperial.ac.uk/media/imperial-college/medicine/sph/ide/gida-fellowships/Imperial-College-COVID19-NPI-modelling-16-03-2020.pdf
    # these rates reflect how many cases in 1000
    rate_of_going_hospital={
        5:1,
        10:3,
        15:3,
        20:12,
        25:12,
        30:32,
        35:32,
        40:49,
        45:49,
        50:102,
        55:102,
        60:166,
        65:166,
        70:243,
        75:243,
        80:273
    }
    rule_applied = False
    destination = person.get_node()
    #If one person is Detected Infected the whole household should be in quarantine
    # we remove this rule as a result of discussion on 21.09
    #if (person.get_status() == Status.Infected_Detected):
    #    person.set_household_quarantined('Home')
    if(person.get_status()==Status.Infected_Detected and person.get_node().get_name() != "Hospital" and person.get_node().get_name() != "ICU"):
        #semi random is specific to a peroson with his/her ID
        semi_random=person.get_ID()%1000
        if (semi_random<=rate_of_going_hospital[int(person.get_age())]):
            destination = 'Hospital'
            rule_applied = True
            return rule_applied, destination
        #else:
        #    destination = 'Home'
        #    rule_applied = True
        #    return rule_applied, destination
    return rule_applied, destination
# END Rule 2
#########################################

#########################################
# Rule 4.
# The recovered should leave the hospital immediately
def rule4(person,t,delta_t):
    rule_applied = False
    destination = person.get_node()
    if ((person.get_status() == Status.Recovered_ID or person.get_status() == Status.Recovered_C)\
            and person.get_node().get_name() == "Hospital"):
        destination = "Home"
        rule_applied = True
        return rule_applied, destination
    return rule_applied, destination
#End Rule 4
#########################################

# Rule 7.
# The recovered should leave the ICU immediately to hospital
def rule7(person,t,delta_t):
    rule_applied = False
    destination = person.get_node()
    if ((person.get_status() == Status.Recovered_ID or person.get_status() == Status.Recovered_C) \
            and person.get_node().get_name() == "ICU"):
        destination = "Hospital"
        rule_applied = True
        return rule_applied, destination
    return rule_applied, destination
# End Rule 7
#########################################

# Rule 9.
# The current node is Home the older people should go outside less according to the parameter
def rule9(person,t,delta_t):
    rule_applied = False
    destination = person.get_node()
    elderly_stay_home    = False
    elderly_go_out_again = False
    if (t>elderly_go_out_again_date and elderly_go_out_again):
        elderly_stay_home = False
    w = random.uniform(0.9, 1.) * person.get_age()
    partial_lockdown_threshold = 65
    if(t>elderly_stay_home_date and elderly_stay_home):
        if(person.get_age()>=65 and w>=partial_lockdown_threshold):
            destination = "Home"
            rule_applied = True
            return rule_applied, destination
    return rule_applied, destination
# END Rule 9
#########################################

#########################################
# Rule 6.
# The people go to ICU if they need critical care depending on their status

def rule6(person,t,delta_t):
    rule_applied = False
    destination = person.get_node()
    # rates are from Ferguson, Neil, et al. "Report 9: Impact of non-pharmaceutical interventions (NPIs) to reduce COVID19 mortality and healthcare demand." (2020).
    # https://www.imperial.ac.uk/media/imperial-college/medicine/sph/ide/gida-fellowships/Imperial-College-COVID19-NPI-modelling-16-03-2020.pdf
    # these rates reflect how many cases in 100
    rate_of_going_ICU = {
        5: 5,
        10: 5,
        15: 5,
        20: 5,
        25: 5,
        30: 5,
        35: 5,
        40: 6,
        45: 6,
        50: 12,
        55: 12,
        60: 27,
        65: 27,
        70: 43,
        75: 43,
        80: 71
    }
    if (person.get_status() == Status.Infected_Detected and \
            person.get_node().get_name() == "Hospital"):
        # semi random is specific to a person with his/her ID
        # and it should be independent of the other two random numbers that crated above so basially it considers the digit 4 to digit 5 of ID
        semi_random = (person.get_ID() / 1000) % 100
        if (semi_random<=rate_of_going_ICU[int(person.get_age())]):
            destination = 'ICU'
            rule_applied = True
    return rule_applied, destination
# END Rule 6
#########################################

def kit_testing(person,t,delta_t):

    rule_applied = False
    destination = person.get_node()

    #everyday do the test at 12
    if (person.get_status() == Status.Infected_Detected and t.hour==12 and t.minute==0):
        test_all_connections(person , t)

    #Check if the home is quarantined and everybody is tested, remove the quarantine
    if (person.is_household_quarantined('Home')):
        for p in person.get_individualized_nodes()['Home'].get_inhabitants():
            #if there is at least one person who is positive nothing changes and the home stay quarantined
            if (p.get_status() == Status.Exposed or
                    p.get_status() == Status.Carrier or
                    p.get_status() == Status.Infected_Detected or
                    p.get_status() == Status.Infected_Undetected):
                return rule_applied, destination
        #if not then unquarantine the home
        person.remove_household_quarantined('Home')
        return rule_applied, destination


    if (person.get_tested_positive()):
        if (person.get_time_passed_from_test()>=test_result_period):
            #quarantine the home
            person.set_household_quarantined('Home')
            rule_applied = True
            destination = 'Home'
            outputfile = open('test_results.out', 'a')
            #long form outputfile.write('Agent %d from node %s is sent to home for quarantine at %s\n'%(person.get_ID(),person.get_node().get_name(),t))
            outputfile.write('%d-%s->q%s\n'%(person.get_ID(),person.get_node().get_name(),t))
            outputfile.close()
            #and switch back to normal to not enter this block again in the future
            person.set_tested_positive(False)
            return rule_applied, destination
        else:
            person.increase_time_passed_from_test(delta_t)

    return rule_applied,destination

#########################################

def self_isolate(person,t,delta_t):

    rule_applied = False
    destination = person.get_node()

    #As we discussed it on 21.09.20 every infected person should stay at home for 14 days.
    time_to_stay_in_quarantine = 14

    #this will ensure that one person is quarantined only once
    if (person.get_status() == Status.Infected_Detected and not person.is_self_isolated() and person.get_time_passed_in_self_isolation()==0.0):
        person.put_self_isolated()
        destination="Home"
        rule_applied=True
        return rule_applied, destination

    if(person.is_self_isolated()):
        if (person.get_time_passed_in_self_isolation() < time_to_stay_in_quarantine):
            person.increase_time_passed_in_self_isolation(delta_t)
        else:
            person.remove_self_isolation()

    return rule_applied, destination

def quarantine_rule_first_degree_contacts(person,t,delta_t):

    rule_applied = False
    destination = person.get_node()

    #As we discussed it on 21.09.20 every infected person should stay at home for 14 days.
    time_to_self_isolate = 14
    time_to_stay_in_quarantine_home = 10

    #this will ensure that one person is quarantined only once
    if (person.get_status() == Status.Infected_Detected and not person.is_self_isolated() and person.get_time_passed_in_self_isolation()==0.0):
        person.put_self_isolated()
        quarantine_all_first_degree_contacts(person)
        destination="Home"
        rule_applied=True
        return rule_applied, destination

    if(person.is_self_isolated()):
        if (person.get_time_passed_in_self_isolation() < time_to_self_isolate):
            person.increase_time_passed_in_self_isolation(delta_t)
        else:
            person.remove_self_isolation()

    if (person.is_first_degree_contact_quarantined()):
        if (person.get_time_passed_in_first_degree_contact_quarantine() < time_to_stay_in_quarantine_home):
            person.increase_time_passed_in_first_degree_contact_quarantine(delta_t)
        else:
            person.remove_first_degree_contact_quarantine()
    return rule_applied, destination

def quarantine_all_first_degree_contacts(person):
    nodes = person.get_individualized_nodes()
    persons = set()
    for node_name, node in nodes.items():
        #will quarantine all the people in the these nodes into quarantine:
        quarantine_nodes=["School","Work Place","Home","Social Event"]
        if(node_name not in quarantine_nodes):
            continue
        persons.update(node.get_inhabitants())
    for p in persons:
        #exclude the person himself/herself
        if(person==p):
            continue
        #if the person is already quarantined will be requarantined
        #but if it is self isolated will do nothing
        if(p.is_self_isolated()):
            continue
        p.put_first_degree_contact_quarantine()

    return

def test_all_connections(infected_person , t):
    nodes = infected_person.get_individualized_nodes()
    outputfile = open('test_results.out', 'a')
    #outputfile.write('%s is infected\n'%infected_person.get_ID())
    test_counter=0
    persons=set()
    for node_name, node in nodes.items():
        #if you want to do the test only in School and Workplace uncomment the following two lines
        if(node_name!="School" and node_name!="Work Place"):
            continue
        persons.update(node.get_inhabitants())
    for person in persons:
        #exclude the person himself/herself
        if(person==infected_person):
            continue
        test_counter+=1
        #outputfile.write('%d-tested-inconnectionwith-%s\n'%(person.get_ID(),node_name))
        if (
                person.get_status() == Status.Carrier or person.get_status() == Status.Infected_Detected or person.get_status() == Status.Infected_Undetected):
            if (~person.get_tested_positive()):

                # long form outputfile.write('Agent %d, with age %d, with status %s is tested positive in node %s%d at time %s\n'
                outputfile.write('%d,%d,%s,%s%d->p%s\n'
                                 % (
                                 person.get_ID(), person.get_age(), person.get_status().name, person.get_node().get_name(),
                                 person.get_node().get_ID(), t)
                                 )
                person.set_tested_positive(True)
    outputfile.write("%d tests done at %s in connection with %s\n"%(test_counter,t,infected_person.get_ID()))
    outputfile.close()


def travel(person,t,delta_t):

    rule_applied = False
    destination = person.get_node()

    travel_start = datetime(2020, 3, 2)
    travel_end = datetime(2022, 8, 28)

    travel_rate = 0.001
    exposed_prob = 0.04
    travel_quarantine_time = 2 #in days

    if(person.is_travel_quarantined() and person.get_time_passed_in_travel_quarantine() >= travel_quarantine_time):
        person.remove_travel_quarantine()
        return rule_applied, destination
    else:
        person.increase_time_passed_in_travel_quarantine(delta_t)

    #travel at 8
    if (person.get_node().get_name() == "Travel"):
        rule_applied = True
        destination = 'Home'
        r = random.random()
        if (r<exposed_prob and person.get_status()==Status.Susceptible):
            person.get_virus_in_travel()

        person.put_travel_quarantine()
        return rule_applied, destination

    if (t>travel_start and t<travel_end and t.hour==8):
        r = random.random()
        if (travel_rate>r
                and person.get_node().get_name() != 'Travel'
                and person.get_status() != Status.Infected_Detected
                and person.get_node().get_name() != 'Hospital'
                and person.get_node().get_name() != 'ICU'
        ):
            rule_applied=True
            destination='Travel'
        return rule_applied, destination

    return rule_applied, destination

def TTI(persons, t, delta_t, TTI_capacity):
    TTI_hour = 23
    if (t.hour!=TTI_hour):
        return

    # As we discussed it on 21.09.20 every infected person should stay at home for 14 days.
    time_to_self_isolate = 14
    time_to_stay_in_quarantine_home = 10

    #n_not_isolated_cases=len([person for person in persons if (person.get_status() == Status.Infected_Detected and not person.is_self_isolated() and person.get_time_passed_in_self_isolation() == 0.0)])
    #print('Not isolated cases are %d at %s'%(n_not_isolated_cases,t))
    #TTI_ratio=1.0
    #if(n_not_isolated_cases!=0):
    #    TTI_ratio=float(TTI_capacity)/n_not_isolated_cases

    n_contacts_of_not_isolated_cases=sum([person.get_number_of_contacts_last_days() for person in persons if (person.get_status() == Status.Infected_Detected and not person.is_self_isolated() and person.get_time_passed_in_self_isolation() == 0.0)])
    print('Contacts of not isolated cases are %d at %s'%(n_contacts_of_not_isolated_cases,t))
    TTI_ratio=1.0
    if(n_contacts_of_not_isolated_cases!=0):
        TTI_ratio=float(TTI_capacity)/n_contacts_of_not_isolated_cases

    for person in persons:
    # this will ensure that one person is quarantined only once
        if ( person.get_status() == Status.Infected_Detected and not person.is_self_isolated() and person.get_time_passed_in_self_isolation() == 0.0):
            person.put_self_isolated()
            quarantine_all_first_degree_contacts_with_probablity(person,TTI_ratio)
            continue

        if ( person.is_self_isolated() ):
            if (person.get_time_passed_in_self_isolation() < time_to_self_isolate):
                person.increase_time_passed_in_self_isolation(delta_t)
            else:
                person.remove_self_isolation()

        if (person.is_first_degree_contact_quarantined()):
            if (person.get_time_passed_in_first_degree_contact_quarantine() < time_to_stay_in_quarantine_home):
                person.increase_time_passed_in_first_degree_contact_quarantine(delta_t)
            else:
                person.remove_first_degree_contact_quarantine()
    return

def quarantine_all_first_degree_contacts_with_probablity(person, ratio):
    nodes = person.get_individualized_nodes()
    persons = set()
    #for contacts in person.get_contacts_of_last_days():
    #    quarantine_nodes = ["School", "Work Place", "Home", "Social Event"]
    #    for k, v in contacts.items():
    #        if (k not in quarantine_nodes):
    #            continue
    #        persons.update(v)
    for node_name, node in nodes.items():
        #will quarantine all the people in the these nodes into quarantine:
        quarantine_nodes=["School","Work Place","Home","Social Event"]
        if(node_name not in quarantine_nodes):
            continue
        persons.update(node.get_inhabitants())
    for p in persons:
        # exclude the person himself/herself
        if (person == p):
            continue
        # if the person is already quarantined will be requarantined
        # but if it is self isolated will do nothing
        if (p.is_self_isolated()):
            continue
        w = random.uniform(0.0,1.0)
        print('want to put %d in q with probability %f and random is %f'%(p.get_ID(),ratio,w))
        if(w<ratio):
            p.put_first_degree_contact_quarantine()

    return


