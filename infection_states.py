from enum import Enum

#All the possible status of one person
class Status(Enum):
    Susceptible = 0
    Carrier = 1
    Infected_Detected = 2
    Infected_Undetected = 7
    Recovered_C = 3
    Recovered_ID = 6
    Recovered_IU = 8
    Dead = 4
    Exposed = 5
    Vaccinated_1_I = 9
    Vaccinated_1_NI = 10
    Vaccinated_2_I = 11
    Vaccinated_2_NI = 12
