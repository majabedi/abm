# Created by Majid Abedi(majabedi@gmail.com) on 12/03/2020
# This file is intended to prepare the simulation and run it. It is an interface between the main runner code
# and the dynamical model.
import pickle

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd

import world
from read_outs import get_all_status_to_report
from person import Status

def drawplot(df, node_name):
    ax = plt.gca()
    for status in get_all_status_to_report():
        df.plot(kind='line', y=node_name + '_' + status, ax=ax, use_index=True, label=status)
    df.plot(kind='line', y = node_name + '_total', ax=ax, use_index=True, label='Total')
    filename = node_name + ".png"
    plt.xlabel('Time')
    plt.ylabel('Number of cases')
    plt.title(node_name)
    plt.tight_layout()
    plt.savefig(filename)
    plt.close('all')


def draw_graph(G):
    from smallworld.draw import draw_network
    ax = plt.gca()
    focal_node = 0
    k_over_2 = 2
    draw_network(G, k_over_2, focal_node=focal_node, ax=ax)
    filename = "graph.png"
    plt.title('Connection graph')
    plt.savefig(filename)
    plt.close('all')


class simulator:
    """
        A class used to create an interface to the model and run the simulation. It creates the plots and reports.
    """

    def __init__(self, world=None):
        """
        initializes the Simulator.
        :param world: if a world object already exists.

        """
        self.world = world
        self.print_graphs = False
        self.report_contacts_flag = False
        self.age_stratified_ICU_Hospital = False
        self.report_statistics_new_cases = False
        self.report_every_n_steps = 1
        self.count_down_to_next_report = 0
        self.initiated_from_file = False

    def initialize_network_explicit_nodes(self, network_file, population_parameters, markov_parameters, individual_interaction_considered = False ):
        """
        initialize a network with node names

        :param network_file: input network file
        :param population_parameters: parameters to create the initial populations
        :param markov_parameters: the markov processes parameters.
        :return:
        """
        self.world = world.world()
        self.world.initialize_network_complete_graph_from_file_individualized_home(network_file=network_file,population_parameters=population_parameters,markov_parameters=markov_parameters, individual_interaction_considered = individual_interaction_considered)
        return

    def save_final_state(self, file):
        self.world.serialize_to_save()
        with open(file, 'wb') as f:
            pickle.dump(self.world,f,pickle.HIGHEST_PROTOCOL)
        return

    def initialize_from_file(self, file):
        with open(file, 'rb') as f:
            self.world = pickle.load(f)
        self.world.deserialize()
        self.initiated_from_file = True
        return


    def with_output(self, file):
        """

        :param file: set the main output file
        """
        self.filename = file
        return

    def print_graphs_at_the_end(self):
        """
        forces the Simulator to print the graphs
        """
        self.print_graphs = True
        return

    def report_contacts(self):
        """
        forces the Simulator to report the number of contacts.
        :return:
        """
        self.report_contacts_flag=True
        return

    def report_new_cases(self):
        """
        forces the Simulator to report the number of new cases
        :return:
        """
        self.report_statistics_new_cases=True
        return

    def report_stratified_ICU_Hospital(self):
        """
        forces the Simulator to report the age stratified ICU and Hospital agents.
        :return:
        """
        self.age_stratified_ICU_Hospital=True
        return

    def set_report_every_n_steps(self, n):
        """
        sets the frequency of reporting statistics.
        :param n: every n time steps
        """
        self.report_every_n_steps = n
        return

    def run(self, initial_time, total_time, delta_t):
        """
        runs the simulation
        :param initial_time: initial time in form of a datetime object
        :param total_time: maximum time in form of a datetime object
        :param delta_t: length of each time step in form of a timedelta object
        :return:
        """
        self.world.set_worldtime(initial_time)
        t = self.world.get_worldtime()

        self.initiate_statistics(t)

        scaling_factor_counter=0
        print('Starting simulation...')
        while t < total_time:
            print('%s/%s' % (t, total_time))

            if (scaling_factor_counter < self.contacts_scaling_info.shape[0]):
                contacts_scaling_date=self.contacts_scaling_info.index[scaling_factor_counter]
                if(contacts_scaling_date==t):
                    contacts_scaling_factor = self.contacts_scaling_info.iloc[scaling_factor_counter]['scaling_factor']
                    contacts_scaling_nodes = self.contacts_scaling_info.iloc[scaling_factor_counter]['nodes']
                    print('At %s scaled contact rates to %f on %s nodes' % (t, contacts_scaling_factor, contacts_scaling_nodes))
                    scaling_factor_counter += 1
                    self.world.scale_all_nodes_contacts(contacts_scaling_factor,contacts_scaling_nodes)

            self.world.evolve(delta_t)
            t = self.world.get_worldtime()
            self.append_statistics(t)


        self.make_report()
        return


    def make_report(self):
        """

        :return: creates all the reports after the simulation
        """
        self.populations.to_csv(self.filename, index_label='t')
        if (self.report_statistics_new_cases):
            self.statistics.to_csv("statistics.csv", index_label='t')
        if (self.report_contacts_flag):
            self.contacts.to_csv("contacts.csv", index_label='t')
        if(self.age_stratified_ICU_Hospital):
            self.age_infection_report_icu['total'] = self.age_infection_report_icu.sum(axis=1)
            self.age_infection_report_hospital['total'] = self.age_infection_report_hospital.sum(axis=1)
            self.age_infection_report_icu.to_csv('ICU-AGE-DIST.csv', index_label='t')
            self.age_infection_report_hospital.to_csv("Hospital-AGE-DIST.csv", index_label='t')
        if (self.print_graphs):
            for node in self.world.get_nodes():
                node_name = node.get_name()
                subdf = self.populations[
                    self.populations.columns[pd.Series(self.populations.columns).str.startswith('%s_' % node_name)]]
                drawplot(subdf, node_name)
            if(self.report_contacts_flag):
                self.contacts.plot(kind='line', use_index=True)
                filename = 'new contacts.png'
                plt.xlabel('Time')
                plt.ylabel('Number of cases')
                plt.tight_layout()
                plt.savefig(filename)
                plt.close('all')


        totals = pd.DataFrame(index=self.populations.index)
        for status in get_all_status_to_report():
            subdf = self.populations[
                self.populations.columns[pd.Series(self.populations.columns).str.endswith('_%s' % status)]]
            totals['total_' + status] = subdf.sum(axis=1)
        subdf = self.populations[
            self.populations.columns[pd.Series(self.populations.columns).str.endswith('_total')]]
        totals['total_total'] = subdf.sum(axis=1)

        if(self.initiated_from_file):
            totals[1:].to_csv("total.csv", index_label='t', mode='a', header=False)
        else:
            totals.to_csv("total.csv", index_label='t')

        drawplot(totals, "total")
        return

    def append_statistics(self, t):
        """
        appends new statistics at time t.
        :param t: current time
        :return:
        """
        if (self.count_down_to_next_report!=0):
            self.count_down_to_next_report-=1
            return
        p = self.world.get_populations()
        self.populations = self.populations.append(pd.DataFrame(p, index=pd.to_datetime([t])))

        if (self.report_statistics_new_cases):
            s = self.world.get_statistics()
            self.statistics = self.statistics.append(pd.DataFrame(s, index=pd.to_datetime([t])))
            f = open('contacts.out', 'a')
            f.write('%s \n' % t)
            for p in self.world.get_persons():
                cont = p.get_contacts()
                for k,v in cont.items():
                    f.write('%d, %s, %d\n' % (p.get_ID(),k,len(v)))
            f.write('\n\n')
            f.close()
            self.world.reset_all_nodes_statistics()
        if (self.report_contacts_flag):
            c = self.world.get_all_contacts()
            self.contacts = self.contacts.append(pd.DataFrame(c, index=pd.to_datetime([t])))
            self.world.reset_contacts()

        if(self.age_stratified_ICU_Hospital):
            age_infection_report_icu = self.world.get_age_statistics("ICU", Status.Infected_Detected)
            age_infection_report_hospital = self.world.get_age_statistics("Hospital", Status.Infected_Detected)
            self.age_infection_report_icu = self.age_infection_report_icu.append(
                pd.DataFrame(age_infection_report_icu, index=[t]))
            self.age_infection_report_hospital = self.age_infection_report_hospital.append(
                pd.DataFrame(age_infection_report_hospital, index=[t]))


        self.count_down_to_next_report = self.report_every_n_steps - 1


    def initiate_statistics(self, t):
        """
        creates the dataframes for reporting the statistics
        :param t: initial time
        """
        p = self.world.get_populations()
        self.populations = pd.DataFrame(p, index=[t])

        if (self.report_statistics_new_cases):
            s = self.world.get_statistics()
            self.statistics = pd.DataFrame(s, index=[t])

        if(self.report_contacts_flag):
            c = self.world.get_all_contacts()
            self.contacts = pd.DataFrame(c, index=[t])

        if(self.age_stratified_ICU_Hospital):
            age_infection_report_icu = self.world.get_age_statistics("ICU", Status.Infected_Detected)
            age_infection_report_hospital = self.world.get_age_statistics("Hospital", Status.Infected_Detected)
            self.age_infection_report_icu = pd.DataFrame(age_infection_report_icu, index=pd.to_datetime([t]))
            self.age_infection_report_hospital = pd.DataFrame(age_infection_report_hospital, index=pd.to_datetime([t]))
        self.count_down_to_next_report = self.report_every_n_steps - 1

    def print_detailed_migration_info(self):
        self.world.print_detailed_migration_info()
        return

    def print_detailed_tracking_info(self):
        self.world.print_detailed_tracking_info()
        return

    def report_initial_agents_nodes(self):
        f = open('agents_nodes.out', 'w+')
        for n in self.world.get_nodes():
            f.write('Created node %s with ID %d\n'%(n.get_name(),n.get_ID()))
        counter=0
        f.write('\n\n')
        for n in self.world.get_individualized_nodes():
            counter +=1
            f.write('Created individualized node %s %d, with ID %d, inhabitants: ['%(n.get_name(),counter,n.get_ID()))
            first_element=True
            for p in n.get_inhabitants():
                if first_element:
                    f.write('%d' % p.get_ID())
                    first_element=False
                else:
                    f.write(',%d' % p.get_ID())
            f.write('] \n')
        f.write('\nAverage population of each home: %f\n'%(1.0*len(self.world.get_persons())/counter))
        f.write('\n')
        counter=0
        for p in self.world.get_persons():
            counter+=1
            f.write('Created person %d with ID: %d, with age: %d, at state: %s\n'%(counter,p.get_ID(),p.get_age(),p.get_status().name))

        f.write('\n')

        f.closed
        return

    def set_contacts_scaling(self,contacts_scaling_info_file):
        if(contacts_scaling_info_file==""):
            self.contacts_scaling_info = pd.DataFrame()
        else:
            self.contacts_scaling_info=pd.read_csv(contacts_scaling_info_file, index_col='Date' )
            self.contacts_scaling_info.index = pd.to_datetime(self.contacts_scaling_info.index)
        return


