#!c:/Python/python.exe
# Created by Majid Abedi at 07/04/2020
# Enter feature description here
# Enter steps here
import glob
import sys
import os
import shutil
import subprocess

import matplotlib
matplotlib.use('Agg')

def run_in_folder(i):
    shutil.rmtree('%d' % i,ignore_errors=True)
    os.mkdir('%d' % i)
    file_list=glob.glob(os.path.join('.', '*.*'))
    for filename in file_list:
        shutil.copy(filename, '%d/' % i)
    os.chdir('%d' % i)
    subprocess.call(["sed","-i","-e","s/seed=10/seed=%d/g"%i,"param.in"])
    subprocess.call(["python3.6","abm.py"])
    for filename in file_list:
        os.remove(filename)
    os.chdir('..')


def main():
    # The default parameter file
    parameter_file = "param.in"
    if (len(sys.argv) > 1):
        n = int(sys.argv[1])
        bunch = n
    if (len(sys.argv) > 2):
        bunch = int(sys.argv[2])
    print('Running %d simulations in bunches of %d' % (n, bunch))

    import multiprocessing as mp
    import math
    for b in range(math.ceil(n/bunch)):
        pool = mp.Pool(mp.cpu_count())
        pool.map(run_in_folder, [i for i in range(b*bunch,min(n,(b+1)*bunch))])
        pool.close()

    plotColumns('total_average.png',n)
    #plotRecoveredWithBraunschweigData('accumulative_recovered.png',n)
    extract_average_new_cases(n)

def extract_average_new_cases(n):
    import pandas as pd

    for i in range(int(n)):
        a = pd.read_csv('%d/statistics.csv' % i, index_col='t', )
        a.index = pd.to_datetime(a.index)
        a = a.groupby(a.index.date).sum()*4.0
        cols = a.columns
        exposed_cols_1 = [c for c in cols if c.endswith('Infected_Detected_1')]
        exposed_cols_2 = [c for c in cols if c.endswith('Infected_Detected_2')]
        a['7day_incidents_1'] = a[exposed_cols_1].sum(axis=1)
        a['7day_incidents_1'] = a['7day_incidents_1'].rolling(7).sum()
        a['7day_incidents_2'] = a[exposed_cols_2].sum(axis=1)
        a['7day_incidents_2'] = a['7day_incidents_2'].rolling(7).sum()
        a.index.name = 't'

        if (i != 0):
            am = pd.concat((am, a))

        else:
            am = a.copy()

    am_std = am.groupby('t').std()
    am = am.groupby('t').mean()

    for c in am.columns:
        am[c+'_std']=am_std[c]

    am.to_csv('statistics_average.csv')
    am[['7day_incidents_1','7day_incidents_1_std','7day_incidents_2','7day_incidents_2_std']].to_csv('7day_statistics_average.csv')

    import matplotlib.pylab as plt
    plt.figure(figsize=(12, 8))
    ax = plt.gca()
    a = pd.read_csv('2020-11-03_rki.csv', index_col='Date')
    a.index = pd.to_datetime(a.index)
    plot_start_date = pd.to_datetime('2020-10-1')
    plot_end_date = pd.to_datetime('2021-11-10')

    a = a[(a.index >= plot_start_date)& (a.index <= plot_end_date)]
    a['weekly.inc'].plot(marker='.', label='Data',ax=ax)
    ax.errorbar(x=am.index, y=am['7day_incidents_1'].values, yerr=am['7day_incidents_1_std'].values, capsize=3,
                label='Variant 1')
    ax.errorbar(x=am.index, y=am['7day_incidents_2'].values, yerr=am['7day_incidents_2_std'].values, capsize=3,
                label='Variant 2')
    plt.legend()
    plt.xlabel('Date')
    plt.ylabel('Weekly incidents')
    plt.savefig('7day_incidents.png')


def plotColumns(filename,n):
    import pandas as pd
    import matplotlib.pylab as plt

    plt.figure(figsize=(12, 8))
    ax = plt.gca()
    for i in range(int(n)):
        a = pd.read_csv('%d/total.csv' % i, index_col='t', )
        a.index = pd.to_datetime(a.index)
        a = a.groupby(a.index.date).mean()
        a.index.name = 't'

        if (i != 0):
            am = pd.concat((am, a))

        else:
            am = a.copy()

    am_std = am.groupby('t').std()
    am = am.groupby('t').mean()

    import matplotlib.colors as mcolors
    cs = [name for name, color in mcolors.BASE_COLORS.items()]
    cs.extend([name for name, color in mcolors.TABLEAU_COLORS.items()])
    cs.extend([name for name, color in mcolors.CSS4_COLORS.items()])
    colors_counter=0

    for c in am.columns:
        am[c+'_std']=am_std[c]
        am.plot(y=c, ax=ax, label=c, legend=True, alpha=1.0, c = cs[colors_counter])
        colors_counter+=1
    am.to_csv('total_average.csv')

    colors_counter = 0
    for i in range(int(n)):
        a = pd.read_csv('%d/total.csv' % i, index_col='t', )
        a.index = pd.to_datetime(a.index)
        a = a.groupby(a.index.date).mean()
        a.index.name = 't'

        if (i != 0):
            a.plot(ax=ax, alpha=0.05, legend=None, label=None, c = cs[colors_counter])

        else:
            a.plot(ax=ax, alpha=0.05, legend=None, label=None, c = cs[colors_counter])

        colors_counter += 1

    plt.xlabel('Time')
    plt.ylabel('Number of cases')

    plt.savefig(filename)
    plt.close('all')

def plotRecoveredWithBraunschweigData(filename,n):
    import pandas as pd
    import matplotlib.pylab as plt

    cols_1 = ['total_Infected_Detected_', 'total_Recovered_ID', 'total_Dead']
    cols_2 = ['total_Infected_Detected', 'total_Recovered_ID', 'total_Dead']
    new_col_name = 'Infected Detected Accumulative'

    plt.figure(figsize=(12, 8))
    ax = plt.gca()

    for i in range(int(n)):
        a = pd.read_csv('%d/total.csv' % i, index_col='t', )
        a.index = pd.to_datetime(a.index)
        a = a.groupby(a.index.date).mean()
        a.index.name = 't'

        if (i != 0):
            a[new_col_name] = a[cols].sum(axis=1)
            am = pd.concat((am, a))
            a[new_col_name].plot(ax=ax, alpha=0.05, legend=False,c='b')

        else:
            a[new_col_name] = a[cols].sum(axis=1)
            am = a.copy()
            a[new_col_name].plot(ax=ax, alpha=0.05, legend=False,c='b')

    am_std = am.groupby('t').std()
    am = am.groupby('t').mean()
    am.plot(kind='line', y=new_col_name, yerr=am_std[new_col_name], ax=ax,capsize=4,c='b')

    bs_data = pd.read_csv('data/BSdata.csv')
    bs_data['Date'] = pd.to_datetime(bs_data.Date)
    bs_data.Date = bs_data.Date - pd.Timedelta(days=4)
    bs_data = bs_data.groupby(bs_data.Date).max()
    start_date = pd.Timestamp('2020-04-1')
    end_date = pd.Timestamp('2020-04-5')
    bs_data = bs_data[(bs_data.index >= start_date) & (bs_data.index <= end_date)]

    normalizatonfactor=25000.0/248000.0
    bs_data=bs_data*normalizatonfactor

    bs_data.plot(marker='.',ax=ax)

    plt.xlabel('Time')
    plt.ylabel(new_col_name)
    plt.savefig(filename)
    plt.close('all')







if __name__ == "__main__":
    main()
