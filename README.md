﻿# ABM COVID-19

## Overview

ABM COVID-19 is a Python software to simulate the spread of COVID-19 infection in a prototypical human society. This project uses an Agent Based Modelling (ABM) framework to study the dynamics of infection in an interacting population. Each agent is a single person which can migrate and carry the infection of get infected by other people. The purpose of this project is to study the effect of different measures which are applied in time, on the spread of the infection.

## Download

The code can be downloaded from the [gitlab repository](https://gitlab.com/majabedi/abm).

## Requirements

To run the code python3+ should be installed. Therefore it is not dependent on the operating system. The other required packages are:

-   [numpy](https://numpy.org/)
-   [pandas](https://pandas.pydata.org/)
-   [matplotlib](https://matplotlib.org/)

## Basic assumptions

The main assumption is that all the dynamics can be categorized under one of the two types i) the infection dynamics and ii) migration dynamics. Each person has one infection status among these states:

-   Susceptible
-   Infected_Detected
- Infected_Undetected
- Exposed
-   Carrier
-   Recovered (from Infected_Detected)
- Recovered (from Infected_Undetected)
-   Recovered (from Carrier)
-   Dead
- Vaccinated once and not Immunized
- Vaccinated once and Immunized
- Vaccinated twice and not Immunized
- Vaccinated twice and Immunized

And at each time point every agent is on one node. More details of the used model could be found in the [pdf file.](https://gitlab.com/majabedi/abm/-/blob/master/Supplementary_Notes_for_the_Agent_Based_Model.pdf)

## How to

Once you have downloaded the files and installed all the requirement you can run the first simulation. You just need to run the following command in the downloaded directory:

```
python3.6 abm.py
```

by default it opens the [param.in](https://gitlab.com/majabedi/abm/-/blob/master/param.in) file and reads the parameters from there, unless the parameter file is explicitly given:

```
python3.6 abm.py parameter_file.in
```

you can of course change the python3.6 to your own version.
This simulation depends on the seed. If you want to reduce this, you can run 
```
python3.6 run_in_parallel.py n
```
where n is the number of parallel runs you want to run. It then runs the simulation for n different seeds.

The code gets several input files and runs the simulation with the provided parameters:

### parameter file (default file is [param.in](https://gitlab.com/majabedi/abm/-/blob/master/param.in))

This parameter file is the main source for parameters. The parameters are defined in the format:

```
parameter = value
```

all the comments are made by # character and all the characters following the # will be ignored. The following parameters should be defined with the following names:

-   _total_time_ is the duration of simulation in days.
-   _delta_t_ is the time step of simulation in minutes.
- _individual_interaction_considered_ is a boolean which defines whether the node considers the individual interaction in a node between two agents.
-   _start_date_ is the start date of simulation in the format (YYYY-MM-DD) like 2019-12-01.
- _contacts_scaling_info_file_ is the path of the file which gives the information about change of contacts rate in different time points according to change in the behavior of people which is not reflect in the migrations. The behavior of people can wearing mask and social distancing. The contact rate is just multiplied by the given factor.
-   _network_file_ is the path of the file which gives the details of migration network of the city.
-   _age_distribution_file_ is the path of the file which includes the age distribution.
- _seed_ is the number for the random number generation for creating the city structure. It is optional, you can just comment it out to get different results every time you run it
- _initial_state_file_ is the path where the simulator can read the initial states of the agents from.
-   _incubation_period_non_infectious,w_sc_i,w_sc_c..._ are the parameters which define the infection dynamics of the model according to the [details](https://gitlab.com/majabedi/abm/-/blob/master/Supplementary_Notes_for_the_Agent_Based_Model.pdf).
- _print_detailed_migration_info_ is a boolean which defines whether it should print the 
-   _print_graphs_ is a boolean which defines whether it should print the produced graphs as png file or not.
-   _report_contacts_ is a boolean which defines whether simulator should report new contacts in the whole network over time as a csv file.
-   _report_age_stratified_ICU_Hospital_ is a boolean which defines whether simulator should report the number of infected people by their age as a csv file or not.
-   _report_statistics_new_cases_ is a boolean which defines whether simulator should report number of newly created cases of each subpopulation in different nodes or not.
- _print_detailed_tracking_info_ is a boolean which defines whether the simulator should print the transmissions as an output file.
-   _report_every_n_steps_ is the frequency of reports, every how many steps a report on the above quantities should be made.

### network file

The network file is given as a formatted JSON text and there are two types of nodes (normal and individualized). A normal node should be of the form:

```
"Hospital":{
      "capacity":5000,
      "initial_total":0,
      "initial_infected_detected":0,
      "initial_exposed":0,
      "death_rate_reduction_factor":1.0,
      "T_mu":44.0,
      "T_sigma":5.0,
      "node_effective_contacts":0
   },
```
-   _capacity_ is simply capacity og the node.
-   _initial_total_ is total population size initially assigned to the node.
-   _initial_infected_detected_ is the number of people who are infected_detected and are in the node.
-   _initial_exposed_ is the number of people who are exposed to the virus and are in the node.
-   _death_rate_reduction_factor_ is the change of death rate, in the node. This will be multiplied by w_id, and be used for this node.
-   _T_mu, T_sigma_ are mean value and standard deviation of minimum time required for an agent to stay on this node. The minimum time is chosen from a log-normal distribution. These parameters will override the T value, if both are given. (in hours)
-   _initial_infected_ is the number of people who are infected are are in the node.
-   _node_effective_contacts_ is the rate of contacts. 

An individualized node is of the form:
```
"Work Place":{
		"capacity":5000,
		"initial_total":0,
		"initial_infected_detected":0,
		"initial_exposed":0,
		"T":6.0,
		"max_allowed_duration":10.0,
		"node_effective_contacts":2,
		"death_rate_reduction_factor":0.0,
		"type":""individualized",
		"min_age":25,
		"max_age":80,
		"node_capacity":16
	},
```
-   _T_ is minimum time required for an agent to stay on this node. (in hours)
- _max_allowed_duration_ is maximum time and agent can stay on this node.(in hours)
- _type_ makes the node an individualized one
- _min_age_ is the minimum age for an agent to migrate to this node.
- _max_age_ is the maximum age for an agent to migrate to this node.
- _node_capacity_ is the maximum number of agents who can migrate to this node.

### age distribution file

This file simply provides the distribution of different ages as a two column csv file. The first column gives the age and the second one gives the normalized cumulative distribution function. The current file is for city [Braunschweig](https://www.citypopulation.de/en/germany/niedersachsen/braunschweig/03101000__braunschweig/)

### contacts_scaling_info

This file provides the information about change of contacts rate in different time points according to change in the behavior of people which is not reflected in the migrations. The behavior of people can be wearing mask and social distancing. The contact rate is just multiplied by the given factor. The first column gives the date, the second the scaling factor and the third on which nodes the scaling is supposed to happen.

### germany_vaccinations_timeseries

This file provides the information about the vaccinations in Germany and is taken from [impfdashboard](https://impfdashboard.de/ )


## Example

You can run the code with the parameters given in the examples folder. It runs a quick simulation for a city of 1000 people for 4 months starting from October 2020. The results will look like the figures below. The outputs in csv format are available in the example folder.
![](example/total.png)
![](example/compartments.png)
*Compartments evolution over time*

### Lockdown

You can apply measured and lockdown for the nodes in the [rulesgathered.py](https://gitlab.com/majabedi/abm/-/blob/master/rulesgathered.py) file. There are several rules that are already there. For example if you just set the values of lockdowns to True, and run the same parameter file in example folder you will see the result below. It shows if you apply full lockdown from each lockdown startdate, how it changes the spread of the infection 
![](example/total-with-lockdown.png)
![](example/compartments.png)
*Compartments evolution over time with lockdown*


## History

The project is started in March 2020 to answer the rising questions of COVID-19 pandemic.

## [](#license)License

[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
