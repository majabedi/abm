from infection_states import Status

def get_all_status_to_report():
    rv = []
    for s in Status:
        if s==Status.Susceptible or s==Status.Vaccinated_1_I or s==Status.Vaccinated_1_NI or s==Status.Vaccinated_2_I or s==Status.Vaccinated_2_NI:
            rv.append(get_unique_name(s,1))
            continue

        for gene in [1,2,3]:
            rv.append(get_unique_name(s,gene))
        #rv.append(get_unique_name(s,1))
    return rv

def get_unique_name(s, variant):
    if s==Status.Susceptible or s==Status.Vaccinated_1_I or s==Status.Vaccinated_1_NI or s==Status.Vaccinated_2_I or s==Status.Vaccinated_2_NI:
        return s.name

    return s.name + '_%d' % variant


