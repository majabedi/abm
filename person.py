# Created by Majid Abedi(majabedi@gmail.com) on 12/03/2020
# This file is intended to have an agent object. It stores all the information about one agent.
# Each person includes: a status and a node.
import random
import sys
import numpy as np

from read_outs import get_unique_name
from infection_states import Status
from collections import deque

class person(object):
    """
    A class used to store all the information about one agent.
    """

    def __init__(self, status, n, incubation_time,age):
        """

        :param status: the status of the agent in the form of an Status.
        :param n: node of the agent, where it is situated.
        :param incubation_time: is the time that a person gets carrier but cannot transfer the infection to the others.
        :param age: age of the person.
        """

        self.set_node(n)

        self.contacts = {}
        self.next_destination=None

        #EXCEPTION:
        #Toconsider the times for Exposed and Carrier, if we set an agent in Carrier state,
        #it needs to know how much the time was in the Exposed state, therefore we set it first to
        #Exposed and then to Carrier
        if (status==Status.Carrier):
            self.status=Status.Exposed
            self.update_next_status_and_time(n)

        self.status = status
        self.update_next_status_and_time(n)

        # If the person is created with a specific state, randomly set the initial spent time on that state to make the
        # states more asynchronous
        w=random.random()
        self.time_passed_in_current_infection_state = self.time_supposed_to_stay_in_infection_state*w

        self.age = age
        self.individualized_nodes = {}
        self.ID = random.randint(0,sys.maxsize)
        self.tested_positive = False
        self.time_passed_from_test = 0
        self.choose_transport()

        self.self_isolated = False
        self.travel_quarantined = False
        self.first_degree_contact_quarantined = False
        self.time_passed_in_self_isolation = 0.0
        self.time_passed_in_travel_quarantine = 0.0
        self.time_passed_in_first_degree_contact_quarantine = 0.0

        self.got_virus_in_travel = False
        self.virus_variant = 1 #1 for ordinary and 2 for VOC(English type), By default it gets the ordinary variant
        n.update_lists_agent_arrived(self)
        #HETEROGENEITY in making contacts to the others
        self.contact_heter=1.0#random.uniform(0.,2.)#.0#np.random.lognormal(-0.34657359027997275,0.8325546111576977)#random.uniform(0,2)
        self.history_of_contacts=deque([])

    def update_next_status_and_time(self, n):
        s, dt = n.update_next_state(self)
        #print('%s,%s,%f'%(self.status.name,s.name,dt))
        self.next_status = s
        self.time_passed_in_current_infection_state=0.0
        self.time_supposed_to_stay_in_infection_state = dt

    def get_status(self):
        """

        :return: the status of the agent
        """
        return self.status

    def set_status(self, new_status):
        """

        :param new_status: sets the status of agent.
        :return:
        """
        self.node.one_changes_status(get_unique_name(self.status,self.virus_variant), get_unique_name(new_status,self.virus_variant))
        self.status = new_status
        return

    def get_ID(self):
        """

        :return: ID of the person
        """
        return self.ID



    def get_node(self):
        """

        :return: the current node.
        """
        return self.node

    def get_time_supposed_to_stay(self):
        """

        :return: the time that the agent are going to stay in current node.
        """
        return self.time_supposed_to_stay


    def get_time_passed_current_node(self):
        """

        :return: the time that the agent has been on the current node.
        """
        return self.time_current_node


    def set_time_passed_current_node(self, t):
        """

        :param t: change the time passed in the current node
        :return:
        """
        self.time_current_node = t
        return


    def increase_time_passed_current_node(self, delta_t):
        """

        :param delta_t: increases the time passes in the current node by delta_t.
        :return:
        """
        self.time_current_node += delta_t
        return


    def get_age(self):
        """

        :return: the age
        """
        return self.age


    def set_node(self, node):
        """

        :param node: the new node
        :return:
        """
        if("mu" in node.get_time_duration().keys()) and ("sigma" in node.get_time_duration().keys()):
            self.time_supposed_to_stay=np.random.lognormal(node.get_time_duration()["mu"],node.get_time_duration()["sigma"],1)
        else:
            if ("T" in node.get_time_duration().keys()):
                self.time_supposed_to_stay = node.get_time_duration()["T"]
            else:
                if ("Tmin" in node.get_time_duration().keys() and "Tmax" in node.get_time_duration().keys()):
                    self.time_supposed_to_stay = random.uniform(node.get_time_duration()["Tmin"],node.get_time_duration()["Tmax"])
                else:
                    raise RuntimeError('You should define a minimum staying time for Node: '+node.get_name())

        self.time_current_node=0
        self.node = node
        return


    def interact_with_population(self,delta_t):
        """
        In this function all the infection interactions with the population on the same node happes.

        :param delta_t: the time step.
        :return:
        """
        _transmitter=None
        if (self.status!=Status.Susceptible and self.status!=Status.Vaccinated_1_NI and self.status!=Status.Vaccinated_2_NI):
            if(self.time_passed_in_current_infection_state<self.time_supposed_to_stay_in_infection_state):
                self.time_passed_in_current_infection_state+=delta_t
            else:
                #print("%s,%s,%f" % (self.status.name, self.next_status.name, self.time_passed_in_current_infection_state))
                self.set_status(self.next_status)
                self.update_next_status_and_time(self.node)
            return False, _transmitter
        else:
            new_status, t, _transmitter = self.node.interact_with_infecteds(self)

            if (t >= delta_t):
                if(self.status==Status.Vaccinated_1_NI or self.status==Status.Vaccinated_2_NI):
                    if(self.time_passed_in_current_infection_state<self.time_supposed_to_stay_in_infection_state):
                        self.time_passed_in_current_infection_state+=delta_t
                    else:

                        self.set_status(self.next_status)
                        self.update_next_status_and_time(self.node)

                return False, _transmitter

            if(new_status==self.status):
                raise RuntimeError('Should not be the same status')
            self.set_virus_variant(_transmitter.get_virus_variant())
            self.set_status(new_status)
            self.update_next_status_and_time(self.node)
            return True, _transmitter

    def interact_with_population_infect_them(self,delta_t):
        """
        In this function all the infection interactions with the population on the same node happes.

        :param delta_t: the time step.
        :return:
        """
        if (self.time_passed_in_current_infection_state < self.time_supposed_to_stay_in_infection_state):
            self.time_passed_in_current_infection_state += delta_t
        else:
            #print("%s,%s,%f" % (self.status.name, self.next_status.name, self.time_passed_in_current_infection_state))
            self.set_status(self.next_status)
            self.update_next_status_and_time(self.node)

        if (self.status==Status.Infected_Detected or self.status==Status.Infected_Undetected or self.status==Status.Carrier):
            infectees, contacts = self.node.interact_infect_others(self, self.contact_heter)
            self.add_to_contacts(contacts, self.node.get_name())

            for infectee in infectees:
                #print("%s,%s,%f" % (infectee.get_status().name, Status.Exposed.name, self.time_passed_in_current_infection_state))
                infectee.get_infected(self.virus_variant)
                infectee.update_next_status_and_time(self.node)
            return infectees
        return []


    def migrate_to(self,destination):
        """
        In this the migration on the nodes happens

        :param destination: the destination node
        :return:
        """
        self.node.update_lists_agent_left(self)
        self.set_node(destination)
        destination.update_lists_agent_arrived(self)
        return


    def set_individualized_nodes(self, inodes):
        """

        :param inodes: sets the individualized node. It should be a map from node name to a node obejct.
        """
        for node_name, node in inodes.items():
            node.add_inhabitant(self)
        self.individualized_nodes = inodes;

    def serialize_to_save(self, inodes):
        self.individualized_nodes = inodes;
        self.node=self.node.get_ID()

    def deserialize(self, nodes_map):
        rv={}
        for k,v in self.individualized_nodes.items():
           rv[k]=nodes_map[v]
        self.individualized_nodes=rv
        self.node=nodes_map[self.node]

    def assign_individualized_node(self, inode):
        """

        :param inode: adds this node to the list of individualized nodes
        """
        inode_name=inode.get_name()
        if (inode_name not in self.individualized_nodes.keys()):
            self.individualized_nodes[inode_name]=inode
            inode.add_inhabitant(self)
        else:
            raise ('Node %s is already defined for person %d'%(inode_name,self.ID))
        return


    def get_individualized_nodes(self):
        """

        :return: returns the individualized nodes dictionary.
        """
        return self.individualized_nodes;

    def set_household_quarantined(self , node_name):
        """
        set one node to quarantine
        :param node_name:
        :return:
        """
        if node_name not in self.individualized_nodes.keys():
            raise RuntimeError('No individualized node with name %s'%node_name)
        self.individualized_nodes[node_name].make_quarantined()
        return

    def remove_household_quarantined(self , node_name):
        """
        set one node to un-quarantine
        :param node_name:
        :return:
        """
        if node_name not in self.individualized_nodes.keys():
            raise RuntimeError('No individualized node with name %s'%node_name)
        self.individualized_nodes[node_name].remove_quarantined_flag()
        return

    def is_household_quarantined(self , node_name):
        """
        :param node_name:
        :return: weather the household are quarantined or not
        """
        if node_name not in self.individualized_nodes.keys():
            raise RuntimeError('No individualized node with name %s'%node_name)
        return self.individualized_nodes[node_name].is_quarantined()

    def get_tested_positive(self):
        """

        :return: the result of the test
        """
        return self.tested_positive

    def get_time_passed_from_test(self):
        """

        :return: the time after test
        """
        return self.time_passed_from_test

    def set_tested_positive(self, flag):
        self.tested_positive = flag
        self.time_passed_from_test = 0
        return

    def increase_time_passed_from_test(self, delta_t):

        self.time_passed_from_test += delta_t
        return

    def is_self_isolated(self):
        return self.self_isolated

    def is_travel_quarantined(self):
        return self.travel_quarantined

    def is_first_degree_contact_quarantined(self):
        return self.first_degree_contact_quarantined

    def get_time_passed_in_self_isolation(self):
        return self.time_passed_in_self_isolation

    def get_time_passed_in_first_degree_contact_quarantine(self):
        return self.time_passed_in_first_degree_contact_quarantine

    def get_time_passed_in_travel_quarantine(self):
        return self.time_passed_in_travel_quarantine

    def increase_time_passed_in_self_isolation(self, delta_t):
        self.time_passed_in_self_isolation+=delta_t

    def increase_time_passed_in_first_degree_contact_quarantine(self, delta_t):
        self.time_passed_in_first_degree_contact_quarantine+=delta_t

    def increase_time_passed_in_travel_quarantine(self, delta_t):
        self.time_passed_in_travel_quarantine+=delta_t

    def put_self_isolated(self):
        self.time_passed_in_self_isolation = 0.0
        self.self_isolated = True
        return

    def put_travel_quarantine(self):
        self.time_passed_in_travel_quarantine = 0.0
        self.travel_quarantined = True
        return

    def put_first_degree_contact_quarantine(self):
        self.time_passed_in_first_degree_contact_quarantine = 0.0
        self.first_degree_contact_quarantined = True

        #missing_rate_test_first_degree_contacts = 1.0
        #self.do_antigen_test(missing_rate_test_first_degree_contacts)
        return

    def remove_self_isolation(self):
        self.self_isolated = False
        self.time_passed_in_self_isolation=0.0
        return

    def remove_travel_quarantine(self):
        missing_rate_test_travel = 1.0
        self.do_antigen_test(missing_rate_test_travel)

        self.time_passed_in_travel_quarantine=0.0
        self.travel_quarantined = False
        return

    def do_antigen_test(self, missing_rate):
        w = random.uniform(0.0, 1.0)
        if (self.status == Status.Carrier and w < (1 - missing_rate)):
            self.set_status(Status.Infected_Detected)
            left_time = self.time_supposed_to_stay_in_infection_state - self.time_passed_in_current_infection_state
            self.update_next_status_and_time(self.node)
            self.time_supposed_to_stay_in_infection_state += left_time
        if (self.status == Status.Infected_Undetected and w < (1 - missing_rate)):
            self.set_status(Status.Infected_Detected)
            left_time = self.time_supposed_to_stay_in_infection_state - self.time_passed_in_current_infection_state
            self.update_next_status_and_time(self.node)
            self.time_supposed_to_stay_in_infection_state = left_time
        return


    def remove_first_degree_contact_quarantine(self):
        self.time_passed_in_first_degree_contact_quarantine = 0.0
        self.first_degree_contact_quarantined = False
        return


    # To test the travel quarantine period

    def has_got_virus_in_travel(self):
        return self.got_virus_in_travel

    def get_virus_in_travel(self):
        self.got_virus_in_travel=True
        self.set_virus_variant(2)
        self.set_status(Status.Exposed)
        self.update_next_status_and_time(self.node)
        w=random.uniform(0.0,1.0)
        #set a random time where the transmission has happened in the last two days of travelling
        self.time_passed_in_current_infection_state = w * self.get_time_passed_current_node()
        
    def get_VOC_virus(self, variant_of_infection = 2):
        self.set_virus_variant(variant_of_infection)
        self.set_status(Status.Exposed)
        self.update_next_status_and_time(self.node)
        #set a random time where the transmission has happened in the last two days of travelling


    def get_time_supposed_to_stay_in_infection_state(self):
        return self.time_supposed_to_stay_in_infection_state

    def reset_contacts(self):
        self.contacts={}

    def get_contacts(self):
        return self.contacts

    def add_to_contacts(self, c, n):
        if n in self.contacts.keys():
            self.contacts[n]=set(self.contacts[n].union(set(c)))
        else:
            self.contacts[n]=set(c)
        return

    def get_infected(self, variant):
        self.set_virus_variant( variant )
        self.set_status( Status.Exposed )
        return

    def get_virus_variant(self):
        return self.virus_variant

    def set_virus_variant(self, var):
        self.node.one_changes_status(get_unique_name(self.status,self.virus_variant), get_unique_name(self.status,var))
        self.virus_variant = var
        return

    def push_to_history_of_contacs(self, last_day_contacts, n_max):
        # last_day_contacts is the contacts of the last day to be added
        # n_max is themaximum number of days that should be saved
        if (len(self.history_of_contacts) >= n_max ):
            self.history_of_contacts.pop()

        self.history_of_contacts.appendleft(last_day_contacts)
        return

    def push_current_contacts_to_history_of_contacs(self, n_max):
        # last_day_contacts is the contacts of the last day to be added
        # n_max is themaximum number of days that should be saved
        if (len(self.history_of_contacts) >= n_max ):
            self.history_of_contacts.pop()

        self.history_of_contacts.appendleft(self.contacts)
        return

    def get_contacts_of_last_days(self):
        return self.history_of_contacts

    def get_number_of_contacts_last_days(self, quarantine_nodes = ["School", "Work Place", "Home", "Social Event"]):
        #counts all the contacts of last days
        rv = 0;
        for contacts in self.history_of_contacts:

            for k, v in contacts.items():
                if (k not in quarantine_nodes):
                    continue
                rv += len(v)

        return rv


    def get_next_destination(self):
        return self.next_destination

    def set_next_destination(self,destination):
        self.next_destination = destination
        return

    def choose_transport(self):
        alpha=random.uniform(0,1)
        p_car=0.7
        if (alpha<p_car):
            self.transport = 'Car'
        else:
            self.transport = 'Public Transport'
            
    def get_transport(self):
        return self.transport
