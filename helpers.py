# Created by Majid Abedi at 17/09/2020
# Helper methods
from person import Status

def extract_transition_parameters(transitions):
    matrix={}
    for s in Status:
        sub_matrix={}
        string_key = 'w-' + s.name
        submap = {key: value for key, value in transitions.items() if key.startswith(string_key)}
        if(len(submap)==0):
            continue
        submap_fractions = {key: value for key, value in transitions.items() if
                            key.startswith(string_key) and key.endswith('fraction')}
        submap_fractions_summation=0
        submap_fractions_summation += sum([float(value) for key, value in submap_fractions.items()])

        #TODO: More sanity checks
        #if (len(submap_fractions) != len(submap) - 1 and submap_fractions_summation != 1.0):
        #    raise RuntimeError('You defined too many fractions for %s transitions')

        for to_status in Status:
            transition_matrix={}
            string_key_complete = string_key + '-' + to_status.name
            one_pair = {key: value for key, value in submap.items() if key.startswith(string_key_complete)}

            if (len(one_pair)==0):
                continue
            else:
                if (string_key_complete + '-fraction' in one_pair.keys()):
                    transition_matrix['fraction'] = float(one_pair[string_key_complete + '-fraction'])
                else:
                    transition_matrix['fraction'] = 1.0 - submap_fractions_summation

                for k,v in one_pair.items():
                    if(not k.endswith('fraction')):
                        if (k.endswith('distribution')):
                            transition_matrix[k.replace(string_key_complete + '-','')] = v
                        else:
                            transition_matrix[k.replace(string_key_complete + '-','')] = float(v)
            sub_matrix[to_status]=transition_matrix
        matrix[s]=sub_matrix
    return matrix

