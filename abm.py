# Created by Majid Abedi(majabedi@gmail.com) o 12/03/2020
# This file handles the main runner of the simulation. It connects to parameter reader and sets
# up a simulator to run.

import random
import sys
from datetime import datetime, timedelta

import numpy.random as nprand

import Simulator
from parameter_reader import read_parameters
from helpers import extract_transition_parameters


def main():
    # The default parameter file
    parameter_file = "param.in"
    if (len(sys.argv) > 1):
        parameter_file = sys.argv[1]
    print('Running the simulation with parameter file: %s' % (parameter_file))

    # Create a map of parameters and values
    parameters = read_parameters(parameter_file)

    # The specifications of population:
    population_parameters = {
        'incubation_time': float(parameters['incubation_period_non_infectious']),
        'age_distribution_file': parameters['age_distribution_file']
    }

    # The parameters to calculate rates of markov transition rates.
    transitions = extract_transition_parameters({ key:value for key, value in parameters.items() if key.startswith('w-')})

    markov_parameters = {
        'w_sc_i': float(parameters['w_sc_i']),
        'w_sc_c': float(parameters['w_sc_c']),
        'transitions': transitions
    }
    delta_t = timedelta(minutes=int(parameters['delta_t']))
    markov_parameters['w_sc_i'] *= int(parameters['delta_t'])/60.
    markov_parameters['w_sc_c'] *= int(parameters['delta_t'])/60.
    
    initial_date=datetime.strptime(parameters['start_date'], '%Y-%m-%d')
    total_time_duration = initial_date + timedelta(days=int(parameters['total_time']))
    if('contacts_scaling_info_file' in parameters.keys()):
        contacts_scaling_info_file=parameters['contacts_scaling_info_file']
    else:
        contacts_scaling_info_file=""

    network_file = parameters['network_file']

    if('seed' in parameters.keys()):
        seed = int(parameters['seed'])
        random.seed(seed)
        nprand.seed(seed)

    simulator = Simulator.simulator()
    simulator.with_output('out.csv')

    individual_interaction_considered = False
    if (parameters['individual_interaction_considered'].lower() == 'true'):
        individual_interaction_considered = True

    if('initial_state_file' in parameters.keys()):
        initial_state_file = parameters['initial_state_file']
        simulator.initialize_from_file(initial_state_file)
    else:
        simulator.initialize_network_explicit_nodes(network_file = network_file, population_parameters=population_parameters ,markov_parameters=markov_parameters, individual_interaction_considered=individual_interaction_considered )

    if (parameters['print_initial_nodes_people'].lower() == 'true'):
        simulator.report_initial_agents_nodes()
    if (parameters['print_detailed_migration_info'].lower() == 'true'):
        simulator.print_detailed_migration_info()
    if (parameters['print_detailed_tracking_info'].lower() == 'true'):
        simulator.print_detailed_tracking_info()
    if (parameters['print_graphs'].lower() == 'true'):
        simulator.print_graphs_at_the_end()
    if (parameters['report_contacts'].lower() == 'true'):
        simulator.report_contacts()
    if (parameters['report_age_stratified_ICU_Hospital'].lower() == 'true'):
        simulator.report_stratified_ICU_Hospital()
    if (parameters['report_statistics_new_cases'].lower() == 'true'):
        simulator.report_new_cases()
    simulator.set_report_every_n_steps(int(parameters['report_every_n_steps']))

    if('seed_dynamics' in parameters.keys()):
        seed_dynamics = int(parameters['seed_dynamics'])
        random.seed(seed_dynamics)
        nprand.seed(seed_dynamics)

    simulator.set_contacts_scaling(contacts_scaling_info_file)

    simulator.run(initial_date,total_time_duration, delta_t)

    if ('final_state_file' in parameters.keys()):
        final_state_file = parameters['final_state_file']
        simulator.save_final_state(final_state_file)
    return


if __name__ == "__main__":
    main()
