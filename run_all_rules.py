# Created by Majid Abedi at 07/04/2020
# Enter feature description here
# Enter steps here
import glob
import sys
import os
import shutil
import subprocess

def run_in_folder(i):
    filelist = glob.glob(os.path.join('.', '*.*'))
    for filename in filelist:
        shutil.copy(filename, '%d/' % i)
    os.chdir('%d' % i)
    subprocess.call(["python3.5", "run_in_parallel.py", "12"])
    for filename in filelist:
        subprocess.call(["rm", filename])
    subprocess.call(["python3.5", "average_visualize.py", "12"])
    os.chdir('..')


def main():
    # The default parameter file
    parameter_file = "param.in"
    if (len(sys.argv) > 1):
        n = sys.argv[1]
    print('Running %d simulations' % (int(n)))

    for i in range(n):
        run_in_folder(i)

if __name__ == "__main__":
    main()
