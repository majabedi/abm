# Created by Majid Abedi(majabedi@gmail.com) on 12/03/2020
# This file is intended to create the whole modelled world. It contains all tje nodes, all the agents and also the time.
import os
import random
from collections import OrderedDict

import numpy as np
import pandas as pd

from all_rules import rules, node_independent_rules
from node import node
from node_individualized import node_individualized
from infection_states import Status
from person import person
from datetime import datetime
from read_outs import get_all_status_to_report


class world:
    """
        A class used to store all properties of the simulated world. It includes agents and nodes.
    """
    def __init__(self):
        self.detailed_migration_report = False
        self.detailed_tracking_report = False
        self.reset_files()
        self.traveler_induced_transmission_home=0
        self.traveler_induced_transmission_nonhome=0
        self.virus_transmitters=[]
        self.variant_switcher = 1
        return

    def reset_files(self):
        # to reset the files
        outputfile = open('test_results.out', 'w+')
        outputfile.close()
        outputfile = open('migrations.out', 'w+')
        outputfile.close()
        outputfile = open('transmissions.out', 'w+')
        outputfile.write("infector,infectee,time\n")
        outputfile.close()
        return


    def evolve(self, delta_t):
        """
        world evolves for timestep delta_t
        :param delta_t: time step in format of deltatime object
        :return:
        """
        dt=delta_t.seconds/86400.
        self.interact_infect_others(dt)  # alternatively you can use interact_get_infection
        self.migrate(dt)
        self.vaccinate(dt)
        if(self.detailed_tracking_report):
            self.print_tracking()

        #self.global_quarantine_rules()

        self.worldtime+=delta_t
        return

    def interact_get_infection(self, delta_t):
        """
        evolves the infection in all the individuals, this function considers the interaction from the perspective of the infected person, meaning the susceptible people make contact
        :param delta_t: time step in days
        :return:
        """
        self.virus_transmitters = []
        for p in random.sample(self.persons, len(self.persons)):
            is_virus_tranmitted, _transmitter = p.interact_with_population(delta_t)
            if is_virus_tranmitted:
                if(not self.individual_interaction_considered):
                    _transmitter=self.give_a_virus_transmitter(p.get_node())
                if  self.detailed_tracking_report:
                    self.virus_transmitters.append([_transmitter,p])
            #To test the travel quarantine period
                if _transmitter.has_got_virus_in_travel():
                    if (p.get_node().get_name()=='Home'):
                        self.traveler_induced_transmission_home += 1
                    else:
                        self.traveler_induced_transmission_nonhome += 1
        return

    def interact_infect_others(self, delta_t):
        """
        evolves the infection in all the individuals, this function considers the interaction from the perspective of the infector person, meaning the infected people make contact to the healthy people
        :param delta_t: time step in days
        :return:
        """
        self.virus_transmitters = []
        for p in random.sample(self.persons, len(self.persons)):
            _infectees = p.interact_with_population_infect_them(delta_t)
            for infectee in _infectees:
                if self.detailed_tracking_report:
                    self.virus_transmitters.append([p,infectee])
                if p.has_got_virus_in_travel():
                    if (p.get_node().get_name()=='Home'):
                        self.traveler_induced_transmission_home += 1
                    else:
                        self.traveler_induced_transmission_nonhome += 1
        return

    def calculate_all_transition_rate_matrices(self):
        """
        calculate the markov transition rates, because some of them are dependent on the different populations.
        """
        for n in self.nodes:
            n.calculate_transition_rate_matrix()
        for n in self.individualized_nodes:
            n.calculate_transition_rate_matrix()
        return

    def reset_all_nodes_statistics(self):
        """
        just counts the different populations
        """
        for n in self.nodes:
            n.reset_statistics()

        #Study travel quarantine
        #For travelling we set up a new variable which stores the new infections by a traveller
        self.traveler_induced_transmission_home=0
        self.traveler_induced_transmission_nonhome=0

        for p in self.persons:
            #TTI days: this should be from parameter file
            tti_days = 3
            p.push_current_contacts_to_history_of_contacs(tti_days)
            p.reset_contacts()


        return

    def reset_contacts(self):
        for n in self.nodes:
            n.reset_counted_contacts()

        return

    def get_populations(self):
        """
        returns a different types of infections and their populations on the node
        :return: returns a dictionary
        """
        populations = {}
        for n in self.nodes:
            for status in get_all_status_to_report():
                populations['%s_%s' % (n.get_name(), status)] = n.get_subpopulation(status)
            populations['%s_total' % (n.get_name())] = n.get_size()
        return populations


    def get_statistics(self):
        """
        returns the statistics of different new cases in different nodes
        :return: returns the statistics
        """
        statistics = {}
        for n in self.nodes:
            for status in get_all_status_to_report():
                statistics['%s_%s' % (n.get_name(), status)] = n.get_statistics(status)

        # To test the travel quarantine period
        statistics['travel_induced_transmission_home'] = self.traveler_induced_transmission_home
        statistics['travel_induced_transmission_nonhome'] = self.traveler_induced_transmission_nonhome
        return statistics

    def print_tracking(self):
        if (not self.detailed_tracking_report):
            return
        tracking_report_file = open('transmissions.out', 'a')
        for transmitter in self.virus_transmitters:
            tracking_report_file.write(
                '%s, %s, %s\n' % (transmitter[0].get_ID(),transmitter[1].get_ID(),self.worldtime))
        tracking_report_file.close()
        return

    def give_a_virus_transmitter(self, node):
        #TODO improve here, no need to search in whole space is it is individualized
        carriers_on_node = [p for p in self.persons if p.get_node() == node and p.get_status() == Status.Carrier]
        infecteds_on_node = [p for p in self.persons if
                           p.get_node() == node and (
                                       p.get_status() == Status.Infected_Undetected or p.get_status() == Status.Infected_Detected)]

        transmission_from_infected_probability = node.get_transmittion_from_infected_probablity()
        transmission_from_carrier_probability = node.get_transmittion_from_carrier_probablity()

        w = random.uniform(0.,1.)
        if (w<transmission_from_infected_probability/(transmission_from_infected_probability+transmission_from_carrier_probability)):
            transmitter = random.sample(infecteds_on_node, 1)
        else:
            transmitter = random.sample(carriers_on_node, 1)

        return transmitter[0]

    def get_nodes(self):
        """
        returns the nodes
        :return: nodes
        """
        return self.nodes

    def get_individualized_nodes(self):
        """
        returns the individualized nodes
        :return: nodes
        """
        return self.individualized_nodes

    def get_node(self,name):
        """
        returns a object node
        :param name: string with the name of the node
        :return: the node object
        """
        for n in self.nodes:
            if (n.get_name()==name):
                return n
        raise ('Trying to migrate to a node which is not defined: %s'%name)
        return None


    def get_age_statistics(self,node_name,status):
        """
        returns the statistics for a specific node and status
        :param node_name: name of the node
        :param status: status
        :return: the statistics
        """
        statistics = {}
        for i in np.arange(5,85,5):
            statistics[i]=0
        for p in self.persons:
            if(p.get_node().get_name()==node_name and p.get_status()==status):
                if (p.get_age() in statistics.keys()):
                    statistics[p.get_age()] += 1
                else:
                    statistics[p.get_age()] = 1
        return statistics

    def get_all_contacts(self):
        """
        returns all contacts in all nodes
        :return: a dictionary of nodes and their personal contacts
        """
        list_of_nodes = self.get_nodes()
        contacts={}
        for n in list_of_nodes:
            contacts[n.get_name()]=n.get_counted_contacts()
        return contacts

    def get_contacts(self,node_name):
        """
        get the new contacts on a single node
        :param node_name: node name
        :return: the number of new contacts
        """
        contacts = 0
        #Add the exception to make it faster
        #Todo: do it in a proper way

        if (node_name!='Home'):
            n=self.get_node(node_name).get_size()
            contacts = self.get_node(node_name).get_node_effective_contacts()
            return int(n*contacts/2)
        for p in self.persons:
            if(p.get_node().get_name()==node_name):
                contacts+=p.get_node().get_size()-1
        return int(contacts/2)

    def set_worldtime(self,t):
        """
        sets the current time of the world
        :param t: time object
        :return:
        """
        self.worldtime = t
        return

    def get_worldtime(self):
        """
        returns the time of the simulation
        :return: the time object
        """
        return self.worldtime

    def get_persons(self):
        """
        returns the people
        :return: the people
        """
        return self.persons



    def migrate(self,delta_t):
        """
        do the migration for all agents for the given time step
        :param delta_t: time step
        :return:
        """
        #p is the person chosen from the whole population randomely
        one_got_VOC=False
        #injection rate starts from 1 and doubles every 14 days
        injection_rate_per_7days_per100k=1
        
        injection_rate=injection_rate_per_7days_per100k/(100000/len(self.persons)*7.0)
        #print('at %s injection rate is %f'%(self.get_worldtime(),injection_rate))
        #injection happens at hour 0 and 1 and 2 ... so we multiple the rate
        injection_times_per_day=1
        injection_rate/=injection_times_per_day

        random_injection_should_happen=random.uniform(0.,1.)<injection_rate
        
        one_got_VOC_2=False
        #injection rate starts from 1 and doubles every 14 days
        injection_rate_per_7days_per100k_2=1
        
        injection_rate_2=injection_rate_per_7days_per100k_2/(100000/len(self.persons)*7.0)
        #print('at %s injection rate is %f'%(self.get_worldtime(),injection_rate))
        #injection happens at hour 0 and 1 and 2 ... so we multiple the rate
        injection_times_per_day_2=1
        injection_rate_2/=injection_times_per_day_2
        
        if(self.get_worldtime()>=datetime(2021, 4, 15) and self.get_worldtime()<=datetime(2021, 7 , 30)):
            random_injection_should_happen_2=random.uniform(0.,1.)<injection_rate_2
        
        for p in random.sample(self.persons, len(self.persons)):
            if(not one_got_VOC and self.get_worldtime()>=datetime(2020, 12, 1) and p.get_status()==Status.Susceptible and self.get_worldtime().hour<injection_times_per_day and self.get_worldtime()<=datetime(2021,3,1) and random_injection_should_happen):
                print("%d got VOC"%(p.get_ID()))
                one_got_VOC=True
                p.get_VOC_virus()
                
            if(not one_got_VOC_2 and not random_injection_should_happen and self.get_worldtime()>=datetime(2021, 4, 15) and p.get_status()==Status.Susceptible and self.get_worldtime().hour<injection_times_per_day and self.get_worldtime()<=datetime(2021,7,30) and random_injection_should_happen_2):
                print("%d got VOC type 2"%(p.get_ID()))
                one_got_VOC_2=True
                p.get_VOC_virus(3)
                
                
            rules_should_apply = rules

            #Here we apply the rules
            #For now we have only one rule defined in another file.

            if (p.get_time_passed_current_node() < p.get_time_supposed_to_stay()):
                rules_should_apply = node_independent_rules

            if(p.get_next_destination() is not None and p.get_time_passed_current_node() >= p.get_time_supposed_to_stay()):
                p.migrate_to(p.get_next_destination())
                if(self.detailed_migration_report):
                            migration_report_file = open('migrations.out', 'a')
                            migration_report_file.write('Agent %d, with age %d, with status %s => %s with ID %d at %s\n'%(p.get_ID(),p.get_age(),p.get_status().name,p.next_destination.get_name(),p.next_destination.get_ID(),self.worldtime))
                            migration_report_file.close()
                p.set_next_destination(None)
            else:
                for rule in rules_should_apply:
                    rule_applied, destination_node_name = rule(person=p,t=self.get_worldtime(),delta_t=delta_t)
                    if(rule_applied):
                        if(destination_node_name in p.get_individualized_nodes().keys()):
                            destination = p.get_individualized_nodes()[destination_node_name]
                        else:
                            destination=self.get_node(destination_node_name)
                        if (destination is not None and destination_node_name!=p.get_node().get_name()):
                            if (p.get_node().get_name() == p.get_transport()):
                                p.migrate_to(destination)
                                if(self.detailed_migration_report):
                                    migration_report_file = open('migrations.out', 'a')
                                    migration_report_file.write('Agent %d, with age %d, with status %s => %s with ID %d at %s\n'%(p.get_ID(),p.get_age(),p.get_status().name,destination.get_name(),destination.get_ID(),self.worldtime))
                                    migration_report_file.close()
                                continue
                            elif (destination.get_name() in ["Hospital", "ICU"]):
                                p.migrate_to(destination)
                                if(self.detailed_migration_report):
                                    migration_report_file = open('migrations.out', 'a')
                                    migration_report_file.write('Agent %d, with age %d, with status %s => %s with ID %d at %s\n'%(p.get_ID(),p.get_age(),p.get_status().name,destination.get_name(),destination.get_ID(),self.worldtime))
                                    migration_report_file.close()
                            else:
                                p.next_destination = destination
                                t = self.get_node(p.get_transport())
                                p.migrate_to(t)
                                if(self.detailed_migration_report):
                                    migration_report_file = open('migrations.out', 'a')
                                    migration_report_file.write('Agent %d, with age %d, with status %s => %s with ID %d at %s\n'%(p.get_ID(),p.get_age(),p.get_status().name,t.get_name(),t.get_ID(),self.worldtime))
                                    migration_report_file.close()
                        continue
            p.increase_time_passed_current_node(delta_t)
        return

    def vaccinate(self, delta_t):
        vaccination_hour = 0
        if(self.get_worldtime().hour!=vaccination_hour):
            return

        a = pd.read_csv('germany_vaccinations_timeseries_v2.tsv', delimiter='\t', index_col='date')
        a.index = pd.to_datetime(a.index)


        daily_vaccines_dose1=0
        daily_vaccines_dose2=0
        if (a.iloc[a.index == self.get_worldtime()]['dosen_erst_differenz_zum_vortag'].size>0):
            daily_vaccines_dose1=a.iloc[a.index == self.get_worldtime()]['dosen_erst_differenz_zum_vortag'][0]
            daily_vaccines_dose2=a.iloc[a.index == self.get_worldtime()]['dosen_zweit_differenz_zum_vortag'][0]
        else:
            if(a.index[-1]<self.get_worldtime() and self.get_worldtime().hour==vaccination_hour ):
                daily_vaccines_dose1 = int(a.iloc[-8:-1]['dosen_erst_differenz_zum_vortag'].mean())
                daily_vaccines_dose2 = int(a.iloc[-8:-1]['dosen_zweit_differenz_zum_vortag'].mean())
                

        
        german_population=83020000.0
        rnd = random.uniform(0, 1)
        if (rnd+int(daily_vaccines_dose1*len(self.persons)/german_population)<daily_vaccines_dose1*len(self.persons)/german_population):
            daily_vaccines_dose1=1+int(daily_vaccines_dose1*len(self.persons)/german_population)
        else:
            daily_vaccines_dose1=int(daily_vaccines_dose1*len(self.persons)/german_population)

        rnd = random.uniform(0, 1)
        if (rnd+int(daily_vaccines_dose2*len(self.persons)/german_population)<daily_vaccines_dose2*len(self.persons)/german_population):
            daily_vaccines_dose2=1+int(daily_vaccines_dose2*len(self.persons)/german_population)
        else:
            daily_vaccines_dose2=int(daily_vaccines_dose2*len(self.persons)/german_population)


        not_vaccinated_75plus=[pp for pp in self.persons if pp.get_age()>=75 and pp.get_status()==Status.Susceptible]
        not_vaccinated_20_75=[pp for pp in self.persons if pp.get_age()>=20 and pp.get_age()<75 and pp.get_status()==Status.Susceptible]
        
        print('Not vaccinateds %d,%d'%(len(not_vaccinated_75plus),len(not_vaccinated_20_75)))

        daily_vaccines_75plus=min(int(daily_vaccines_dose1/2),len(not_vaccinated_75plus))
        
        latest_full_75plus_vaccination_date = datetime(2021,3,15)
        if (self.get_worldtime()<=latest_full_75plus_vaccination_date):
            daily_vaccines_75plus = daily_vaccines_dose1
            

        daily_vaccines_75minus=int(daily_vaccines_dose1-daily_vaccines_75plus)

        

        if (daily_vaccines_dose1>0):
            print('at %s should get %d, %d, %d' % (self.get_worldtime(), daily_vaccines_dose1, daily_vaccines_75plus, daily_vaccines_75minus))


            daily_vaccinateds_75plus=0
            daily_vaccinateds_75minus=0
            
            random_sequence_75plus=random.sample(not_vaccinated_75plus, min(int(daily_vaccines_75plus),len(not_vaccinated_75plus)))
            for p in random_sequence_75plus:
                    p.set_status(Status.Vaccinated_1_NI)
                    p.update_next_status_and_time(p.get_node())
                    daily_vaccinateds_75plus+=1
            print("%d 75plus people vaccinated at %s"%(daily_vaccinateds_75plus,self.get_worldtime()))
            
            print ('ohoy %d %d'%(len(not_vaccinated_20_75),daily_vaccines_75minus))
            
            random_sequence_75minus=random.sample(not_vaccinated_20_75, min(daily_vaccines_75minus,len(not_vaccinated_20_75)))
            for p in random_sequence_75minus:
                    p.set_status(Status.Vaccinated_1_NI)
                    p.update_next_status_and_time(p.get_node())
                    daily_vaccinateds_75minus+=1
            print("%d 75minus people vaccinated at %s"%(daily_vaccinateds_75minus,self.get_worldtime()))
            
       
        return

    def print_detailed_migration_info(self):
        self.detailed_migration_report=True

    def print_detailed_tracking_info(self):
        self.detailed_tracking_report=True

    def initialize_network_complete_graph_from_file_individualized_home(self, network_file,
                                                                        population_parameters,
                                                                        markov_parameters,
                                                                        individual_interaction_considered):
        """
        initializes the network for the world from a file
        :param network_file: the network file
        :param population_parameters: parameters for population
        :param markov_parameters: parameters for markov processes
        :return:
        """
        import json
        _network = json.load(open(network_file), object_pairs_hook=OrderedDict)
        nodes = _network.keys()
        self.nodes = []
        self.individualized_nodes = []
        self.persons = []
        self.markov_parameters=markov_parameters
        self.individual_interaction_considered = individual_interaction_considered
        age_dependent_rates = self.override_age_dependent_rates()
        print('Initializing the network...')
        _incubation_time = population_parameters['incubation_time']
        for n in nodes:
            if (n.startswith("-")):
                continue
            if (_network[n]["capacity"] is not None):
                c = _network[n]["capacity"]
            else:
                raise RuntimeError("The capacity for this node is not given. I will stop.")
            print('Reading node ' + n + ' from file...')

            initial_total ,initial_populations = self.read_initial_populations(_network[n])
            exposed = initial_populations[Status.Exposed]
            carrier = initial_populations[Status.Carrier]
            infected_detected = initial_populations[Status.Infected_Detected]
            infected_undeteced = initial_populations[Status.Infected_Undetected]

            node_effective_contacts = 100000000 #infinit number of contacts by default
            if ("node_effective_contacts" in _network[n].keys()):
                node_effective_contacts = _network[n]["node_effective_contacts"]
            death_rate_reduction_factor = 1.
            if ("death_rate_reduction_factor" in _network[n].keys()):
                death_rate_reduction_factor = _network[n]["death_rate_reduction_factor"]
            duration = 0
            if ("T" in _network[n].keys()):
                duration = {
                    "T": _network[n]["T"] / 24.
                }

            if ("Tmax" in _network[n].keys() and "Tmin" in _network[n].keys()):
                duration = {
                    "Tmax": _network[n]["Tmax"] / 24.,
                    "Tmin": _network[n]["Tmin"] / 24.
                }

            if (n == 'ICU' or n == 'Hospital'):
                duration = np.random.exponential(duration)
            #Put a normal distribution for the times staying in a node
            if ("T_mu" in _network[n].keys() and "T_sigma" in _network[n].keys()):
                duration = {
                    "mu": _network[n]["T_mu"] / 24.,
                    "sigma" : _network[n]["T_sigma"] / 24.
                }

            # basically infinite
            max_allowed_duration = 1000000
            if ("max_allowed_duration" in _network[n].keys()):
                max_allowed_duration = _network[n]["max_allowed_duration"] / 24.
                # TODO should be converted to days
            age_dist = np.genfromtxt(population_parameters['age_distribution_file'], delimiter=',', skip_header=1)

            if (n == "Home"):

                self.create_individualized_homes(_incubation_time, age_dependent_rates, age_dist, c,
                                                 death_rate_reduction_factor, duration, exposed, carrier, infected_detected,
                                                 infected_undeteced, initial_populations, initial_total,
                                                 markov_parameters, max_allowed_duration, n, node_effective_contacts,
                                                 individual_interaction_considered = individual_interaction_considered)

            elif ('type' in _network[n].keys() and _network[n]['type']=="individualized"):
                if ("min_age" in _network[n].keys() and "max_age" in _network[n].keys() and "node_capacity" in  _network[n].keys()):
                    min_age = _network[n]['min_age']
                    max_age = _network[n]['max_age']
                    node_capacity = _network[n]['node_capacity']
                else:
                    raise ('You must define the node_capacity, min_age and max_age for individualized node %s'%n)
                #TODO: make this condition more strong
                if (exposed!=0 or infected_detected!=0):
                    raise ('In this version, all the population should be at home initially which is not the case for %s'%n)
                self.create_individualized_node(_incubation_time, age_dependent_rates, age_dist, c, death_rate_reduction_factor,
                                 duration, exposed, infected_detected, infected_undeteced, initial_populations, initial_total,
                                 markov_parameters, max_allowed_duration, n, node_effective_contacts, individual_interaction_considered, min_age=min_age, max_age=max_age, node_capacity=node_capacity)
            else:
                #TODO: make this condition more strong
                if (exposed!=0 or infected_detected!=0):
                    raise ('In this version, all the population should be at home initially which is not the case for %s'%n)
                self.create_node(_incubation_time, age_dependent_rates, age_dist, c, death_rate_reduction_factor,
                                 duration, exposed, infected_detected, initial_populations, initial_total,
                                 markov_parameters, max_allowed_duration, n, node_effective_contacts, individual_interaction_considered = individual_interaction_considered)

        #self.select_initial_from_ages(5, 5)
        #self.select_initial_from_nodes("School")

        return

    # use this function if you want to initially confine the virus holders to a certain age group
    def select_initial_from_ages(self, min_age, max_age):
        sub_group = [p for p in self.persons if (min_age <= p.get_age() and p.get_age() <= max_age)]
        sub_group = random.sample(sub_group,len(sub_group))
        sub_group_virus = [p for p in self.persons if (
            p.get_status() == Status.Infected_Detected or
            p.get_status() == Status.Infected_Undetected or
            p.get_status() == Status.Exposed
        )]
        if (len(sub_group_virus)>len(sub_group)):
            raise ('Your specified age group population is less that the initial virus holders')
        for i in range(len(sub_group_virus)):
            tmp_status=sub_group[i].get_status()
            sub_group[i].set_status(sub_group_virus[i].get_status())
            sub_group_virus[i].set_status(tmp_status)

    # use this function if you want to initially confine the virus holders to a certain node(s)
    def select_initial_from_nodes(self, node_name):
        node_list = [n for n in self.individualized_nodes if (n.get_name()==node_name)]
        sub_group = []
        for n in node_list:
            sub_group.extend(n.get_inhabitants())

        sub_group_virus = [p for p in self.persons if (
                p.get_status() == Status.Infected_Detected or
                p.get_status() == Status.Infected_Undetected or
                p.get_status() == Status.Exposed
        )]

        if (len(sub_group_virus) > len(sub_group)):
            raise ('Your specified age group population is less that the initial virus holders')
        for i in range(len(sub_group_virus)):
            tmp_status = sub_group[i].get_status()
            sub_group[i].set_status(sub_group_virus[i].get_status())
            sub_group_virus[i].set_status(tmp_status)

    def create_node(self, _incubation_time, age_dependent_rates, age_dist, c, death_rate_reduction_factor, duration,
                    exposed, infected_detected, initial_populations, initial_total, markov_parameters,
                    max_allowed_duration, n, node_effective_contacts, individual_interaction_considered):
        new_node = node(name=n, duration=duration, max_allowed_duration=max_allowed_duration,
                        node_effective_contacts=node_effective_contacts,
                        initial_populations=initial_populations,
                        capacity=c,
                        markov_parameters=markov_parameters,
                        death_rate_reduction_factor=death_rate_reduction_factor,
                        age_dependent_rates=age_dependent_rates,
                        individual_interaction_considered = individual_interaction_considered)
        print('The time duration to stay in %s' % (n), duration)
        print('Created node %s with total %d people,%d infected, %d exposed, and capacity %d.' % (
            n, initial_total, infected_detected, exposed, c))
        for j in range(infected_detected):
            rnd = random.uniform(0, 1)
            age = age_dist[np.searchsorted(age_dist[:, 1] > rnd, True), 0]
            incubation_time = np.random.exponential(_incubation_time)
            p = person(Status.Infected, new_node, incubation_time, age)
            p.set_virus_variant(self.variant_switcher)
            self.switch_variant()
            self.persons.append(p)
        for j in range(exposed):
            rnd = random.uniform(0, 1)
            age = age_dist[np.searchsorted(age_dist[:, 1] > rnd, True), 0]
            incubation_time = np.random.exponential(_incubation_time)
            p = person(Status.Exposed, new_node, incubation_time, age)
            p.set_virus_variant(self.variant_switcher)
            self.switch_variant()
            self.persons.append(p)
        for j in range(initial_total - infected_detected - exposed):
            rnd = random.uniform(0, 1)
            age = age_dist[np.searchsorted(age_dist[:, 1] > rnd, True), 0]
            incubation_time = np.random.exponential(_incubation_time)
            p = person(Status.Susceptible, new_node, incubation_time, age)
            p.set_virus_variant(self.variant_switcher)
            self.switch_variant()
            self.persons.append(p)
        new_node.reset_statistics()
        self.nodes.append(new_node)

    def create_individualized_homes(self, _incubation_time, age_dependent_rates, age_dist, c,
                                    death_rate_reduction_factor, duration, exposed, carrier, infected_detected,
                                    infected_undeteced, initial_populations, initial_total, markov_parameters,
                                    max_allowed_duration, n, node_effective_contacts,
                                    individual_interaction_considered = False):
        """

        :type infected_detected: object
        """
        new_parent_node = node(name=n, duration=duration, max_allowed_duration=max_allowed_duration,
                               node_effective_contacts=node_effective_contacts, initial_populations=initial_populations,
                               capacity=c,
                               markov_parameters=markov_parameters,
                               death_rate_reduction_factor=death_rate_reduction_factor,
                               age_dependent_rates=age_dependent_rates,
                               is_parent=True,
                               individual_interaction_considered = individual_interaction_considered)
        samples = random.sample(range(initial_total), infected_detected + infected_undeteced + exposed + carrier)
        exposed_people = samples[0:exposed]
        infected_detected_people = samples[exposed:exposed + infected_detected]
        infected_undetected_people = samples[
                                     exposed + infected_detected:exposed + infected_detected + infected_undeteced]
        carrier_people = samples[
                                     exposed + infected_detected + infected_undeteced : exposed + infected_detected + infected_undeteced + carrier]
        # number of already allocated people
        j = 0
        while j < initial_total:

            # create initial population of the home with mean of 1.7 and minimum 1 person
            house_population = np.random.binomial(5, 0.14) + 1
            if (j + house_population) > initial_total:
                house_population = initial_total - j

            exposeds_current_home = 0
            infected_detected_current_home = 0
            infected_undetected_current_home = 0
            carriers_current_home = 0
            for ip in range(house_population):
                j += 1
                if (j in exposed_people):
                    exposeds_current_home += 1
                if (j in infected_detected_people):
                    infected_detected_current_home += 1
                if (j in infected_undetected_people):
                    infected_undetected_current_home += 1
                if (j in carrier_people):
                    carriers_current_home += 1

            initial_populations = {
                Status.Infected_Detected: infected_detected_current_home,
                Status.Infected_Undetected: infected_undetected_current_home,
                Status.Carrier: carriers_current_home,
                Status.Susceptible: house_population - infected_detected_current_home - infected_undetected_current_home - exposeds_current_home,
                Status.Dead: 0,
                Status.Exposed: exposeds_current_home,
                Status.Recovered_C: 0,
                Status.Recovered_ID: 0,
                Status.Recovered_IU: 0
            }
            # TODO Fix the capacity
            new_node = node_individualized(name=n, global_node=new_parent_node, duration=duration,
                                           max_allowed_duration=max_allowed_duration,
                                           node_effective_contacts=node_effective_contacts,
                                           initial_populations=initial_populations,
                                           capacity=c,
                                           markov_parameters=markov_parameters,
                                           death_rate_reduction_factor=death_rate_reduction_factor,
                                           age_dependent_rates=age_dependent_rates,
                                           individual_interaction_considered=individual_interaction_considered)
            j -= house_population
            for ip in range(house_population):
                j += 1
                status = Status.Susceptible
                if (j in exposed_people):
                    status = Status.Exposed
                if (j in infected_detected_people):
                    status = Status.Infected_Detected
                if (j in infected_undetected_people):
                    status = Status.Infected_Undetected
                if (j in carrier_people):
                    status = Status.Carrier
                rnd = random.uniform(0, 1)
                age = age_dist[np.searchsorted(age_dist[:, 1] > rnd, True), 0]
                incubation_time = np.random.exponential(_incubation_time)
                p = person(status, new_node, incubation_time, age)
                p.set_virus_variant(self.variant_switcher)
                self.switch_variant()
                p.set_individualized_nodes(
                    {
                        n: new_node
                    }
                )
                self.persons.append(p)

            new_node.reset_statistics()
            self.individualized_nodes.append(new_node)
        self.nodes.append(new_parent_node)


    def create_individualized_node(self, _incubation_time, age_dependent_rates, age_dist, c,
                                    death_rate_reduction_factor, duration, exposed, infected_detected,
                                    infected_undeteced, initial_populations, initial_total, markov_parameters,
                                    max_allowed_duration, n, node_effective_contacts, individual_interaction_considered, min_age = 5 ,max_age = 80, node_capacity = 20):
        new_parent_node = node(name=n, duration=duration, max_allowed_duration=max_allowed_duration,
                               node_effective_contacts=node_effective_contacts, initial_populations=initial_populations,
                               capacity=c,
                               markov_parameters=markov_parameters,
                               death_rate_reduction_factor=death_rate_reduction_factor,
                               age_dependent_rates=age_dependent_rates,
                               is_parent=True,
                               individual_interaction_considered = individual_interaction_considered)
        #define the a random sequence of assigning people to nodes
        sub_group=[p for p in self.persons if (min_age<=p.get_age() and p.get_age()<=max_age)]
        samples = random.sample(sub_group, len(sub_group))
        new_node_required=True
        for p in samples:
            if new_node_required:
                initial_populations = {
                    Status.Infected_Detected: 0,
                    Status.Infected_Undetected: 0,
                    Status.Carrier: 0,
                    Status.Susceptible: 0,
                    Status.Dead: 0,
                    Status.Exposed: 0,
                    Status.Recovered_C: 0,
                    Status.Recovered_ID: 0,
                    Status.Recovered_IU: 0
                }
                new_node = node_individualized(name=n, global_node=new_parent_node, duration=duration,
                                               max_allowed_duration=max_allowed_duration,
                                               node_effective_contacts=node_effective_contacts,
                                               initial_populations=initial_populations,
                                               capacity=c,
                                               markov_parameters=markov_parameters,
                                               death_rate_reduction_factor=death_rate_reduction_factor,
                                               age_dependent_rates=age_dependent_rates,
                                               individual_interaction_considered=individual_interaction_considered)
                new_node_required=False
                j = 0
                self.individualized_nodes.append(new_node)
            j=j+1
            p.assign_individualized_node(new_node)
            if j==node_capacity:
                new_node_required=True
            new_node.reset_statistics()
        self.nodes.append(new_parent_node)


    def read_initial_populations(self, node):
        if (node["initial_total"] is not None):
            initial_total = node["initial_total"]
        else:
            raise RuntimeError("The initial_total for this node is not given. I will stop.")

        initial_populations = {
            Status.Susceptible: initial_total
        }
        for s in Status:
            if s==Status.Susceptible:
                continue
            key_name = 'initial_'+ s.name.lower()
            value=0
            if (key_name in node.keys()):
                value += node[key_name]
            initial_populations[s]=value
            initial_populations[Status.Susceptible]-=value


        return initial_total, initial_populations

    def override_age_dependent_rates(self):

        import pandas as pd
        rates_file = 'rate-age-dependent.csv'
        if (os.path.exists(rates_file)):
            return pd.read_csv(rates_file, index_col='age')
        return None

    def scale_all_nodes_contacts(self, a, nodes):
        for n in self.nodes:
            if (n.get_name() in nodes or nodes == "all"):
                n.scale_all_contacts_frequency(a)
        for n in self.individualized_nodes:
            if (n.get_name() in nodes or nodes == "all"):
                n.scale_all_contacts_frequency(a)

    def serialize_to_save(self):
        for p in self.persons:
            rv={}
            for k, v in p.get_individualized_nodes().items():
                rv[k] = v.get_ID()
            p.serialize_to_save(rv)
        return

    def deserialize(self):
        rv={}
        for n in self.nodes:
            rv[n.get_ID()] = n

        for n in self.individualized_nodes:
            rv[n.get_ID()] = n

        for p in self.persons:
            p.deserialize(rv)
        return

    def switch_variant(self):
        return
        if (self.variant_switcher==1):
            self.variant_switcher=2
        else:
            self.variant_switcher=1

    def global_quarantine_rules(self):
        delta_t=1.0
        TTI_capacity=2.0
        TTI(self.persons,self.worldtime,delta_t,TTI_capacity)
        return

