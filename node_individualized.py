# Created by Majid Abedi(majabedi@gmail.com) on 25/03/2020
# This file is intended to have a node which is individualized, to prevent the agents share a common place
# This is very important when the lock down is applied and the agents go to their individual homes.

from node import node

class node_individualized(node):
    """
       A class used to store the individualized nodes. It inherits all properties of node object and has one prent node.
       The parent node hold the global statistics.
       For example, each individualized home is one object of node_individualized and they have a common parent node.
       Since it does not make sense to store the each individual home statistics, we store all of them in the parent node.
    """

    def __init__(self, global_node, name, capacity, duration, max_allowed_duration, markov_parameters, node_effective_contacts, initial_populations, death_rate_reduction_factor = 1 , age_dependent_rates =None, individual_interaction_considered = False):
        """
        initializes the object

        :param global_node: the parent node to store the global statistics
        :param name: name of the node as string
        :param capacity: capacity of the node
        :param duration: minimum duration required for agents to stay on this node in days
        :param max_allowed_duration: maximum time that agents can spend on this node in days
        :param markov_parameters: markov transition rates
        :param node_effective_contacts: number of effective contacts
        :param initial_populations: initial number of each population
        :param death_rate_reduction_factor: how much the death rate should reduce in this node
        """
        self.global_node = global_node
        self.quarantined = False
        self.inhabitants = []
        super().__init__(name, capacity, duration, max_allowed_duration, markov_parameters,
                     node_effective_contacts, initial_populations=initial_populations,
                     death_rate_reduction_factor=death_rate_reduction_factor,
                     age_dependent_rates = age_dependent_rates,
                     individual_interaction_considered = individual_interaction_considered)

    def one_changes_status(self, old_status, new_status):
        """
        to inform the node that one agent has changed the infection status. It also changes the status in the parent node.
        :param old_status: old status
        :param new_status: new status
        :return:
        """
        self.global_node.one_changes_status(old_status,new_status)
        super().one_changes_status(old_status,new_status)
        return

    def change_one_subpopulation_and_total(self, status, delta):
        """
        to inform the node that one agent has migrated from/to the node. it also informs the parent node.
        :param status: the infection status of the migrating agent
        :param delta: the change.
        :return:
        """
        self.global_node.change_one_subpopulation_and_total(status, delta)
        super().change_one_subpopulation_and_total(status, delta)
        return

    def make_quarantined(self):
        """
        makes one home quarantined
        :return:
        """
        self.quarantined = True
        return

    def remove_quarantined_flag(self):
        """
        makes one home un-quarantined
        :return:
        """
        self.quarantined = False
        return

    def is_quarantined(self):
        """

        :return: whether the home is quarantined or not
        """
        return self.quarantined

    #For testing reasons we need to have the information of inhabitants stored somewehre
    def add_inhabitant(self, person):
        self.inhabitants.append(person)
        return

    def get_inhabitants(self):
        return self.inhabitants

    def increase_counted_contacts(self, n):
        self.global_node.increase_counted_contacts(n)
        #For now we do not want to count the contacts of a small node
        #self.counted_contacts += n
        return




