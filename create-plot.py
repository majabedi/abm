# Created by Majid Abedi at 07/04/2020
# Enter feature description here
# Enter steps here
import glob
import sys
import os
import shutil
import subprocess

def run_in_folder(i,jobs):
    filelist = glob.glob(os.path.join('.', '*.*'))
    for filename in filelist:
        shutil.copy(filename, '%d/' % i)
    os.chdir('%d' % i)
    subprocess.call(["python3.6", "run_in_parallel.py", jobs])
    subprocess.call(["python3.6", "average_visualize.py", jobs])
    for filename in filelist:
        subprocess.call(["rm", filename])
    os.chdir('..')


def main():
    # The default parameter file
    parameter_file = "param.in"
    if (len(sys.argv) > 1):
        directory = sys.argv[1]
        n = int(sys.argv[2])


    import pandas as pd
    import matplotlib.pylab as plt

    import os
    os.chdir(directory)




    # here we define to draw the plots every 20 rows from 0 to 180*4(we have 4 timepoints for every day)
    #How many time points you have in the total.csv:
    total_timepoints_reported=180 * 4
    #Draw the histogram every every_n time steps:
    every_n = 20
#1- HISTOGRAMS

    for j in range(0, total_timepoints_reported, every_n):


        plt.figure(figsize=(12, 8))
        ax = plt.gca()
        for i in range(int(n)):
            a = pd.read_csv('%d/total.csv' % i, index_col='t', )
            a.index = pd.to_datetime(a.index)
            a = a['total_Recovered_I'] + a['total_Infected_Detected'] + a['total_Infected_Undetected'] + a['total_Dead']

            if (i != 0):
                am = pd.concat((am, a))

            else:
                am = a.copy()

        plt.figure(figsize=(12, 8))
        ax = plt.gca()
        am[am.index == a.index[j]].groupby('t').hist(bins=10, figsize=(4, 3), ax=ax)
        plt.title(a.index[j])
        plt.savefig('%d.png' % j)

        plt.close('all')

# 2- AUTPCORELATIONS
        import matplotlib.colors as mcolors
        import numpy as np

        cs = [name for name, color in mcolors.BASE_COLORS.items()]
        cs.remove('w')
        cs.extend([name for name, color in mcolors.TABLEAU_COLORS.items()])
        cs.extend([name for name, color in mcolors.CSS4_COLORS.items()])


        fig, [ax1, ax2, ax3] = plt.subplots(3, 1, figsize=(15, 25))

        for i in range(int(n)):
            a = pd.read_csv('%d/total.csv' % i, index_col='t', )
            a.index = pd.to_datetime(a.index)
            a = np.log(a['total_Recovered_I'] + a['total_Infected_Detected'] + a['total_Infected_Undetected'] + a[
                'total_Dead'])

            ax1.plot(a, label='%d' % i, color=cs[i])
            ax1.legend()
            ax1.set_title('infected')

            b = pd.read_csv('%d/out.csv' % i, index_col='t', )
            b.index = pd.to_datetime(b.index)
            filter_col = [col for col in b.columns if col.endswith('Infected_Detected')]

            # ax2.plot(b[filter_col].sum(axis=1),label='%d'%i,color=cs[i])
            # ax2.plot(b[filter_col].sum(axis=1).rolling(window=28).mean(),label='%d'%i,color=cs[i],alpha=0.5)
            # ax2.set_title('new Infected_Detected')

            # b3fluc=b[filter_col].sum(axis=1)-b[filter_col].sum(axis=1).rolling(window=8).mean()
            # ax3.plot((b3fluc.abs().rolling(window=8).mean()),label='%d'%i,color=cs[i])
            averaging_period = 28
            ax2.plot((a.rolling(averaging_period).apply(lambda x: x.autocorr(), raw=False)), label='%d' % i, color=cs[i])
            ax2.set_title('Autocorrelation of Log(infected) in timeframes of 7 days')

            ax3.plot((a.rolling(averaging_period).apply(lambda x: x.autocorr(), raw=False)), label='%d' % i, color=cs[i])
            ax3.set_title('Autocorrelation of Log(infected) in timeframes of 7 days')
            ax3.set_ylim(0.95, 1)

        plt.savefig('log-babis.png')
        plt.close('all')

# 3- FLUCTUATIONS

        fig, [ax1, ax2, ax3] = plt.subplots(3, 1, figsize=(15, 25))

        # here we define to draw the plots every 20 rows from 0 to 180*4(we have 4 timepoints for every day)
        # for j in tqdm(range(0,4*180,2*180)):

        for i in range(int(n)):
            a = pd.read_csv('%d/total.csv' % i, index_col='t', )
            a.index = pd.to_datetime(a.index)
            a = a['total_Recovered_I'] + a['total_Infected_Detected'] + a['total_Infected_Undetected'] + a['total_Dead']

            ax1.plot(a, label='%d' % i, color=cs[i])
            ax1.legend()
            ax1.set_title('infected')

            b = pd.read_csv('%d/out.csv' % i, index_col='t', )
            b.index = pd.to_datetime(b.index)
            filter_col = [col for col in b.columns if col.endswith('Infected_Detected')]

            ax2.plot(b[filter_col].sum(axis=1), label='%d' % i, color=cs[i])
            # take the average every averaging_period_2 timesteps
            averaging_period_2 = 28
            ax2.plot(b[filter_col].sum(axis=1).rolling(window=averaging_period_2).mean(), label='%d' % i, color=cs[i], alpha=0.5)
            ax2.set_title('new Infected_Detected')

            # take the average every averaging_period_3 timesteps for thirs time step
            averaging_period_3 = 8
            b3fluc = b[filter_col].sum(axis=1) - b[filter_col].sum(axis=1).rolling(window=averaging_period_3).mean()
            ax3.plot((b3fluc.abs().rolling(window=averaging_period_3).mean()), label='%d' % i, color=cs[i])
            # ax3.plot((b[filter_col].sum(axis=1).rolling(28).apply(lambda x: x.autocorr(), raw=False)),label='%d'%i,color=cs[i])
            ax3.set_title('Moving average of distance between new cases and Moving Average of new cases')

        plt.savefig('signals.png')


if __name__ == "__main__":
    main()
