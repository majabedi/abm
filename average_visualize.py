# Created by Majid Abedi at 07/04/2020
# Enter feature description here
# Enter steps here
import glob
import sys
import os
import shutil
import subprocess

def run_in_folder(i):
    shutil.rmtree('%d' % i,ignore_errors=True)
    os.mkdir('%d' % i)
    for filename in glob.glob(os.path.join('.', '*.*')):
        shutil.copy(filename, '%d/' % i)
    os.chdir('%d' % i)
    subprocess.call(["python3.5", "abm.py"])
    os.chdir('..')


def main():
    # The default parameter file
    parameter_file = "param.in"
    if (len(sys.argv) > 1):
        n = sys.argv[1]
    print('Running %d simulations' % (int(n)))

    import pandas as pd
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt


    ax = plt.gca()
    for i in range(int(n)):
        a = pd.read_csv('%d/total.csv'%i)



        if (i!=0):
            am=pd.concat((am,a))
            a['total_Infected'].plot(ax=ax,c='r', alpha=0.1)
            a['total_Recovered_I'].plot(ax=ax, c='b',alpha=0.1)
            a['total_Dead'].plot(ax=ax, c='g',alpha=0.1)
        else:
            am=a.copy()
            a.plot(kind='line', y='total_Infected', ax=ax, color='red', label='total_Infected',alpha=0.1)
            a.plot(kind='line', y='total_Recovered_I', ax=ax, color='blue', label='total_Recovered_I',alpha=0.1)
            a.plot(kind='line', y='total_Dead', ax=ax, color='green', label='total_Dead',alpha=0.1)



    am_std=am.groupby('t').std()
    am=am.groupby('t').mean()
    am['total_Infected_std']=am_std['total_Infected']
    am['total_Recovered_I_std']=am_std['total_Recovered_I']
    am['total_Dead_std']=am_std['total_Dead']


    am.to_csv('total_average.csv')

    am.plot(kind='line', y='total_Infected', ax=ax, color='red', label='simulation mean')
    am.plot(kind='line', y='total_Recovered_I', ax=ax, color='blue', label='simulation mean')
    am.plot(kind='line', y='total_Dead', ax=ax, color='green', label='simulation mean')


    ax.set_xlim(0, 15)
    ax.set_ylim(0, 150)


    plt.xlabel('Time')
    plt.ylabel('Number of cases')


    plt.savefig('total_average.png')
    plt.close('all')








if __name__ == "__main__":
    main()
